/**
 * Created by mac on 2017/1/12.
 */

// config
var config = require('config.json')('setting.json');

// twilio
var twiliolib = require('twilio')(config.twilioSMS.accountSid, config.twilioSMS.authToken);

function SMS() {
}

SMS.send = function (message, to) {

    /**
     * @params message : 要傳送的訊息內容
     * @params options : 相關設定
     *   @params from : 系統人員電話
     *   @params to : 客戶的電話
     *
     * **/
    twiliolib.messages.create({
        body: message,
        to: to.replace(/^0/, "+886"),
        from: config.twilioSMS.from
        // mediaUrl: 'http://www.yourserver.com/someimage.png'
    }, function(err, data) {
        if (err) {
            console.error('Could not notify administrator');
            console.error(err);
        } else {
            console.log('Administrator notified');
        }
    });
};

module.exports = SMS;
//exports.SMSSend = SMSSend;