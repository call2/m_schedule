/**
 * Created by mac on 2017/1/12.
 */

var FCM = require('fcm-push');  //Android
var apn = require('apn');   //iOS
var config = require('config.json')('setting.json');

function PushService() {

}

PushService.AndroidPush = function (UUID, Alert) {
    var apiKey = config.Push.AndroidAPIKey;
    var fcm = new FCM(apiKey);
    var tokens = UUID;
    // console.log(fcm);
    var message = {
        to: tokens, // required
        // collapse_key: 'your_collapse_key',
        // data: {
        //     your_custom_data_key: 'your_custom_data_value'
        // },
        notification: {
            //title: 'Title of your push notification',
            body: Alert
        }
    };



    //callback style
    fcm.send(message, function(err, response){
        if (err) {
            console.log(err);
            console.log("Something has gone wrong!");
        } else {
            console.log("Successfully sent with response: ", response);
        }
    });
};


PushService.iOSPush = function (UUID, Alert,notReadCount) {

    // var pem = "./services/MuzkyPushDeveloper.pem";
    // var key = "./services/MuzkyPushDeveloper.pem";
    var pem = "./services/MuzkyPushStore.pem";
    var key = "./services/MuzkyPushStore.pem";

    var tokens = UUID;
    var service = new apn.Provider({
        "cert": pem,
        "key": key,
        "passphrase":"55555555",
        "production": false
    });

    var note = new apn.Notification({
        "alert": Alert,
        "sound":"default"
        //"sound":"cat2.wav"
    });

    //note.topic = "com.ghostmapit.googhost";
    note.badge = notReadCount;

    //console.log("Sending: ${note.compile()} to ${tokens}");
    // service.send(note, tokens).then( (result) => {
    //     console.log("sent:", result.sent.length);
    //     console.log("failed:", result.failed.length);
    //     console.log(result.failed);
    // });

    service.send(note, tokens);

    service.shutdown();

    //openssl x509 -in cert.cer -inform DER -outform PEM -out cert.pem  匯出push cer
    //openssl pkcs12 -in sportsStore.p12 -out sportsStore.pem -nodes    //匯出push cer的 key
    //Enter Import Password: (輸入你剛剛匯出私鑰的密碼)
    //gateway.sandbox.push.apple.com
    //gateway.push.apple.com
    //openssl s_client -connect gateway.push.apple.com:2195 -cert push.pem -key key.pem //檢測憑證與鑰匙是否有效
    // The topic is usually the bundle identifier of your application.

    // For one-shot notification tasks you may wish to shutdown the connection
    // after everything is sent, but only call shutdown if you need your
    // application to terminate.
};

module.exports = PushService;