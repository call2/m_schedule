/**
 * Created by mac on 2017/2/24.
 */

var nodemailer = require('nodemailer');
//var apn = require('apn');   //iOS
var config = require('config.json')('setting.json');

function Mail() {

}

Mail.send = function (ToMail, Subject, Content, Ccmail) {

    var account = config.Mail.GmailAccount;
    var password = config.Mail.GmailPassword;

    //宣告發信物件
    var transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: account,
            pass: password
        }
    });

    var options = {
        //寄件者
        from: account,
        //收件者
        to: ToMail,
        //副本
        cc: Ccmail,
        //密件副本
        bcc: '',
        //主旨
        subject: Subject, // Subject line
        //純文字
        //text: Content // plaintext body
        //嵌入 html 的內文
        html: Content
        //附件檔案
        // attachments: [ {
        //     filename: 'text01.txt',
        //     content: '聯候家上去工的調她者壓工，我笑它外有現，血有到同，民由快的重觀在保導然安作但。護見中城備長結現給都看面家銷先然非會生東一無中；內他的下來最書的從人聲觀說的用去生我，生節他活古視心放十壓心急我我們朋吃，毒素一要溫市歷很爾的房用聽調就層樹院少了紀苦客查標地主務所轉，職計急印形。團著先參那害沒造下至算活現興質美是為使！色社影；得良灣......克卻人過朋天點招？不族落過空出著樣家男，去細大如心發有出離問歡馬找事'
        // }, {
        //     filename: 'unnamed.jpg',
        //     path: '/Users/Weiju/Pictures/unnamed.jpg'
        // }]
    };

    //發送信件方法
    transporter.sendMail(options, function(error, info){
        if(error){
            console.log(error);
        }else{
            console.log('訊息發送: ' + info.response);
        }
    });

};

module.exports = Mail;