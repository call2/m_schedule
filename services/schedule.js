/**
 * Created by ralph on 8/31/2017.
 */

// var express = require('express');
// var router = express.Router();

//PushNotification
var pushNotification = require('../lib/userSendPush');
//Errors
var errors = require('../lib/errors');
//box logger
var boxlogger = require('../lib/boxloger');
//db
var models  = require('../models');
// config
var config = require('config.json')('setting.json');
// winston logging
var winston = require('winston');
// moment: date/time format
var moment = require('moment-timezone');
// util: like a C++ printf format
var util = require('util');
var tool=require('../lib/tool')
//
var schedulejob = require('node-schedule');
//twilio
var Twilio=require('twilio');
var AccessToken = require('twilio').jwt.AccessToken;
var VideoGrant = AccessToken.VideoGrant;
var TWILIO_ACCOUNT_SID = "ACecd161f0ff875c37ebb0345cbd4f653b";
var TWILIO_API_KEY = "SKb69172501a113b4a82867b79fe005523";
var TWILIO_API_SECRET ="ZgkiZNfPW9m4LE5oygUl0zOAVjHvP0Bc";

var wqcloud = require('../services/wqcloud');


function schedule() {

}

schedule.Start=function(){
    schedulejob.scheduleJob('*/5 * * * *', function () {
        console.log(new Date(), '***********schedule in every 5 minutes******');
        //上課前10分鐘，系統發送推播給老師告知老師準備上課
        // console.log("*****1. 上課前10分鐘，系統發送推播給老師告知老師準備上課");
        var ymd=moment().tz("Asia/Taipei").format(tool.YMD);
        var chkdate=moment(tool.setDate(ymd));
        console.log("****chkdate:"+chkdate);
        // models.Courseunit.findAll({where:{State:null,Unitdate:chkdate},include:[{model:models.Classlog}]}).then(function (docs) {
        models.Courseunit.findAll({where:{State:null},include:[{model:models.Classlog}]}).then(function (docs) {
            if (docs!=null && docs.length>0){
                // console.log("上課前10分鐘共有:"+docs.length.toString()+" 筆資料");
                docs.forEach(function (unit) {
                    models.Course.findOne({where:{id:unit.CourseId,Coursetype:["S","M"],State:"Y",Auditstate:"Y"},include:[{model:models.Teacher}]}).then(function (course) {
                        if (course!=null){
                            var course=course;
                            // console.log([course.Unitno,course.Unit]);
                            if (course.Unitno<course.Unit){ //排除掉已上完課的...
                                //比對時間
                                var hm=(course.Coursetime==null)?"":course.Coursetime;
                                var unitdatestr=moment(unit.Unitdate).tz("Asia/Taipei").format(tool.YMD)+" "+hm;
                                // console.log([unit.Unitdate,unitdatestr])
                                var unidate=moment(unitdatestr,"YYYY.MM.DD HH:mm").tz("Asia/Taipei");
                                // var unidate=tool.setDate(moment(unit.Unitdate).tz("Asia/Taipei").format(tool.YMD)+" "+(course.Coursetime==null)?"":course.Coursetime);
                                var now=moment().tz("Asia/Taipei").add(15,"Minutes");
                                // console.log([unidate,now])
                                if (unidate<=now){
                                    //狀態改為W
                                    models.Courseunit.update({State:"W"},{where:{id:unit.id}}).then(function () {
                                        //給老師送推播
                                        if (course.Teacher!=null ){
                                            //	圖片：該課程圖片 	標題： (課程名稱)即將上課 	內容： 按下Ready開始你的課程
                                            pushNotification.sendPushByOne_SavePush(course.Teacher.MemberId,unit.CourseId,"B","("+course.Title+")即將上課",course.Picurl,"("+course.Title+")即將上課,按下Ready開始你的課程",function (ok) {
                                                // if (ok) console.log("送出推播:"+course.Teacher.MemberId);
                                                //產生學生的classlog
                                                models.Purchaselog.findAll({where:{Status:"Y",CourseId:unit.CourseId}}).then(function (log) {
                                                    if (log!=null && log.length>0){
                                                        log.forEach(function (tmp) {
                                                            models.Classlog.findOne({where:{MemberId:tmp.MemberId,CourseunitId:unit.id}}).then(function (classlog) {
                                                                if (classlog==null){
                                                                    models.Classlog.create({Status:"Y",CourseunitId:unit.id,MemberId:tmp.MemberId}).then(function () {
                                                                        // console.log("產生上課紀錄:"+unit.id+" memberid:"+tmp.MemberId);
                                                                    })
                                                                }
                                                                //發送學生上課通知 XX 不需要...老師進入直播室 會發推播給學生
                                                                // pushNotification.sendPushByOne_SavePush(tmp.MemberId,unit.CourseId,"B","("+course.Title+")即將上課",course.Picurl,"按下Ready開始你的課程");
                                                            })
                                                        })
                                                    }
                                                })
                                            })
                                        }
                                    })
                                }
                            }
                        }
                    })
                })
            }
        })

        // console.log("*****2. 檢查VIP到期");
        var chkdate7_0=moment().add(7,"Days");
        var chkdate7_1=moment().add(7,"Days").add(5,"Minutes");
        var chkdate1_0=moment().add(1,"Days");
        var chkdate1_1=moment().add(1,"Days").add(5,"Minutes");
        console.log([chkdate7_0,chkdate7_1,chkdate1_0,chkdate1_1]);
        models.Member.findAll({where:{$or:[{$and:[{Vipduedate:{ $gte:chkdate7_0}},{Vipduedate:{$lt:chkdate7_1}}]},{$and:[{Vipduedate:{ $gte:chkdate1_0}},{Vipduedate:{$lt:chkdate1_1}}]}]}}).then(function (mbr) {
            if (mbr!=null && mbr.length>0){
                // console.log("檢查VIP到期:共"+mbr.length+"筆")
                for(var i=0;i<mbr.length;++i){
                    //圖片：MUZKY LOGO       標題：您的VIP會員有效期限即將到期！   內容：您的VIP會員有效期限即將於XXXX年XX月XX日到期，若要繼續享有VIP權益別忘了到個人頁面延長囉！
                    var duedate=moment(mbr[i].Vipduedate).tz("Asia/Taipei").format("YYYY年MM月DD日");
                    var memberid=mbr[i].id;
                    // console.log("memberid:"+memberid)
                    pushNotification.sendPushByOne_SavePush(memberid,null,"G","您的VIP會員有效期限即將到期！","","您的VIP會員有效期限即將於"+duedate+"到期，若要繼續享有VIP權益別忘了到個人頁面延長囉",function (ok) {
                        // if (ok) console.log("送出推播:"+memberid);
                    })
                }
            }
        })

        // console.log("*****3.VIP到期改為Memberlevel=0 及優惠卷State=N ");
        var now=moment();
        models.Member.findAll({where:{$and:[{Vipduedate:{$ne:null}},{Vipduedate:{$lt:now}},{Memberlevel:"1"}]}}).then(function (docs) {
            if (docs!=null && docs.length>0){
                // console.log(" VIP到期 改為Memberlevel=0 cnt:"+docs.length);
                for (var i=0;i<docs.length;++i){
                    docs[i].Memberlevel="0";
                    docs[i].save().then(function () {
                    })
                }
            }
        })
        models.Coupon.findAll({where:{$and:[{Duedate:{$lt:now}},{State:"Y"}]}}).then(function (docs) {
            console.log("優惠到期:"+docs.length)
            if (docs!=null && docs.length>0){
                // console.log(" VIP到期 改為Memberlevel=0 cnt:"+docs.length);
                for (var i=0;i<docs.length;++i){
                    docs[i].State="N";
                    docs[i].save().then(function () {
                    })
                }
            }
        })

        // console.log("*****4. 檢查課程超過10分鐘未結束");
        models.Courseunit.findAll({where:{State:["W","R","Y"],Unitdate:{$lte:chkdate}}}).then(function (docs) {
            if (docs!=null && docs.length>0){
                console.log("檢查課程超過10分鐘未結束:共"+docs.length);
                console.log(docs);
                docs.forEach(function (unit) {
                    models.Course.findOne({where:{id:unit.CourseId,Coursetype:["S","M"]}}).then(function (course) { //,State:"Y"
                        if (course!=null){
                            //比對時間
                            var coursetype=course.Coursetype;
                            var datestr=moment(unit.Unitdate).tz("Asia/Taipei").format("YYYY-MM-DD");//+" "+(course.Coursetime==null)?" 00:00":course.Coursetime;
                            datestr+=(course.Coursetime==null)?" 00:00":" "+course.Coursetime;
                            // console.log([unit.Unitdate,course.Coursetime,datestr]);
                            var unidate=moment(datestr).add(course.Courseduration+10,"Minutes") ;
                            // console.log("+++duration plus")
                            // console.log(course.Courseduration+10);
                            var now=moment().tz("Asia/Taipei");
                            if (unidate<=now){
                                // console.log("*********unit.id******:"+unit.id);
                                // console.log([unidate,now]);
                                // var filename='M'+parseInt(unit.id).toLocaleString('en-US', {minimumIntegerDigits: 7, useGrouping:false});
                                // console.log("----filename:"+filename)
                                // 狀態改為E
                                models.Courseunit.update({State:"E"},{where:{id:unit.id}}).then(function () {
                                    course.Unitno+=1;
                                    course.save().then(function () {
                                        if (coursetype=="S"){  //關閉 twilio直播
                                            if (course.Unitno>=course.Unit)
                                            {
                                                course.State="E"; course.save();
                                            }
                                            var client = new Twilio(TWILIO_API_KEY, TWILIO_API_SECRET, {accountSid: TWILIO_ACCOUNT_SID});
                                            client.video.rooms(unit.Roomsid)
                                                .update({
                                                    status: 'completed'
                                                }).then(function (room) {
                                                // console.log("room close:"+ unit.Roomsid);
                                            }).catch(function (err) {
                                                console.log("room close FAIL:"+unit.Roomsid);
                                                console.log(err.message);
                                            })
                                        }
                                        else {  //關閉 騰訊
                                            // var filename='M'+parseInt(unit.id).toLocaleString('en-US', {minimumIntegerDigits: 7, useGrouping:false});
                                            // console.log("----filename:"+filename)
                                            var channelid=wqcloud.bizId+"_"+unit.Roomid;
                                            var starttime=moment(unit.Starttime).tz("Asia/Taipei").format("YYYY-MM-DD HH:mm:ss");
                                            var filename=unit.Roomid;
                                            var unitid=unit.id;
                                            wqcloud.Live_Tape_Stop(channelid,unit.Roomsid,function () {
                                                wqcloud.Live_Channel_Close(channelid,function () {
                                                    wqcloud.Live_Tape_GetFilelist(channelid,starttime,function (res) {
                                                        var tmp=JSON.parse(res);
                                                        models.Recordfile.create({logstr:JSON.stringify(tmp),CourseunitId:unitid}).then(function (record) {
                                                            var video=tool.filterData(tmp.output.file_list,{file_format: "3"});
                                                            if (video!=null && video.length>0){
                                                                var files=[],urls=[];
                                                                for (var i=0;i<video.length;++i){
                                                                    files.push(video[i].record_file_id);
                                                                    urls.push(video[i].record_file_url);
                                                                }
                                                                if (files.length==1){
                                                                    unit.Videourl=urls[0];
                                                                    unit.save();
                                                                    record.state="Y";
                                                                    record.save();
                                                                    tool.sendPushFinishVideo(unitid);
                                                                }
                                                                else{
                                                                    wqcloud.ConcatVideo(files,filename,function (res) {
                                                                        if (res["code"]==0){
                                                                            record.state="C";
                                                                            record.save();
                                                                        }
                                                                        wqcloud.DescribeVodPlayInfo(filename,function (res0) {
                                                                            if (res0["code"]==0){
                                                                                var videourl="";
                                                                                for (var i=0;i<res0["fileSet"].length;++i){
                                                                                    if (res0["fileSet"][i]["playSet"][0].url.indexOf('.mp4')>0){
                                                                                        videourl=res0["fileSet"][i]["playSet"][0].url;
                                                                                        break;
                                                                                    }
                                                                                }
                                                                                unit.Videourl=videourl;
                                                                                unit.save().then(function () {
                                                                                })
                                                                                if (videourl!="") {
                                                                                    record.state="Y";
                                                                                    record.save();
                                                                                    tool.sendPushFinishVideo(unitid);
                                                                                }
                                                                            }
                                                                        })
                                                                    })
                                                                }
                                                            }
                                                        })

//回傳json格式
//{"errmsg":"return successfully!","message":"return successfully!","output":{"all_count":16,
// "file_list":[{"appid":"1254094533","create_time":"2017-11-03 17:58:10","end_time":"2017-11-03 17:58:09","err_code":"0","expire_time":"2038-01-19 11:14:07","file_format":"3","file_id":"9031868223470773625","file_size":"2038169","id":"418998126","record_file_id":"9031868223470773625","record_file_url":"http://1254094533.vod2.myqcloud.com/72067003vodgzp1254094533/2c4510289031868223470773625/f0.mp4","record_type":null,"report_message":null,"start_time":"2017-11-03 17:57:20","stream_id":"10906_M-8og1j9i419ok","task_id":null,"task_sub_type":"0","vid":"210012913_bf443861eba7404eb6e00b667877f177","vod2Flag":"1"},{"appid":"1254094533","create_time":"2017-11-03 17:58:09","end_time":"2017-11-03 17:58:09","err_code":"0","expire_time":"2038-01-19 11:14:07","file_format":"1","file_id":"9031868223471358586","file_size":"2031537","id":"418998061","record_file_id":"9031868223471358586","record_file_url":"http://1254094533.vod2.myqcloud.com/72067003vodgzp1254094533/6fc933ce9031868223471358586/f0.flv","record_type":null,"report_message":null,"start_time":"2017-11-03 17:57:23","stream_id":"10906_M-8og1j9i419ok","task_id":null,"task_sub_type":"0","vid":"210012913_3eae48bb6cfe4574adf3fc51baa9b767","vod2Flag":"1"},{"appid":"1254094533","create_time":"2017-11-03 17:58:10","end_time":"2017-11-03 17:58:09","err_code":"0","expire_time":"2038-01-19 11:14:07","file_format":"1","file_id":"9031868223471160510","file_size":"2031537","id":"418998091","record_file_id":"9031868223471160510","record_file_url":"http://1254094533.vod2.myqcloud.com/72067003vodgzp1254094533/6b2c97189031868223471160510/f0.flv","record_type":null,"report_message":null,"start_time":"2017-11-03 17:57:25","stream_id":"10906_M-8og1j9i419ok","task_id":null,"task_sub_type":"0","vid":"210012913_5daca1f261594cd2ade6af64b57cd9df","vod2Flag":"1"},{"appid":"1254094533","create_time":"2017-11-03 17:58:10","end_time":"2017-11-03 17:58:09","err_code":"0","expire_time":"2038-01-19 11:14:07","file_format":"1","file_id":"9031868223470773626","file_size":"1694638","id":"418998122","record_file_id":"9031868223470773626","record_file_url":"http://1254094533.vod2.myqcloud.com/72067003vodgzp1254094533/2c4510299031868223470773626/f0.flv","record_type":null,"report_message":null,"start_time":"2017-11-03 17:57:28","stream_id":"10906_M-8og1j9i419ok","task_id":null,"task_sub_type":"0","vid":"210012913_8ecc2faaad3545e397c76d8234c5a42d","vod2Flag":"1"},{"appid":"1254094533","create_time":"2017-11-03 17:58:09","end_time":"2017-11-03 17:58:09","err_code":"0","expire_time":"2038-01-19 11:14:07","file_format":"1","file_id":"9031868223471358585","file_size":"1694638","id":"418998058","record_file_id":"9031868223471358585","record_file_url":"http://1254094533.vod2.myqcloud.com/72067003vodgzp1254094533/6fc933cd9031868223471358585/f0.flv","record_type":null,"report_message":null,"start_time":"2017-11-03 17:57:29","stream_id":"10906_M-8og1j9i419ok","task_id":null,"task_sub_type":"0","vid":"210012913_10576b2dbaea4f569eb839e61220b23e","vod2Flag":"1"},{"appid":"1254094533","create_time":"2017-11-03 18:01:53","end_time":"2017-11-03 18:01:50","err_code":"0","expire_time":"2038-01-19 11:14:07","file_format":"3","file_id":"9031868223471062363","file_size":"8452571","id":"419006132","record_file_id":"9031868223471062363","record_file_url":"http://1254094533.vod2.myqcloud.com/72067003vodgzp1254094533/68d87f5f9031868223471062363/f0.mp4","record_type":null,"report_message":null,"start_time":"2017-11-03 17:58:22","stream_id":"10906_M-8og1j9i419ok","task_id":null,"task_sub_type":"0","vid":"210012913_594d60c5c929443f8c653aa6b53b5273","vod2Flag":"1"},{"appid":"1254094533","create_time":"2017-11-03 18:01:51","end_time":"2017-11-03 18:01:50","err_code":"0","expire_time":"2038-01-19 11:14:07","file_format":"1","file_id":"9031868223471114368","file_size":"8427712","id":"419006084","record_file_id":"9031868223471114368","record_file_url":"http://1254094533.vod2.myqcloud.com/72067003vodgzp1254094533/6ad446429031868223471114368/f0.flv","record_type":null,"report_message":null,"start_time":"2017-11-03 17:58:25","stream_id":"10906_M-8og1j9i419ok","task_id":null,"task_sub_type":"0","vid":"210012913_6fbd9cd2c01e47c698ed12c72b6ba505","vod2Flag":"1"},{"appid":"1254094533","create_time":"2017-11-03 18:01:52","end_time":"2017-11-03 18:01:50","err_code":"0","expire_time":"2038-01-19 11:14:07","file_format":"1","file_id":"9031868223471114369","file_size":"8427712","id":"419006096","record_file_id":"9031868223471114369","record_file_url":"http://1254094533.vod2.myqcloud.com/72067003vodgzp1254094533/6ad446439031868223471114369/f0.flv","record_type":null,"report_message":null,"start_time":"2017-11-03 17:58:27","stream_id":"10906_M-8og1j9i419ok","task_id":null,"task_sub_type":"0","vid":"210012913_631cd3a67329482590352c426d1bb1d0","vod2Flag":"1"},{"appid":"1254094533","create_time":"2017-11-03 18:02:19","end_time":"2017-11-03 18:02:18","err_code":"0","expire_time":"2038-01-19 11:14:07","file_format":"3","file_id":"9031868223470774231","file_size":"753306","id":"419007056","record_file_id":"9031868223470774231","record_file_url":"http://1254094533.vod2.myqcloud.com/72067003vodgzp1254094533/2c458ba29031868223470774231/f0.mp4","record_type":null,"report_message":null,"start_time":"2017-11-03 18:02:01","stream_id":"10906_M-8og1j9i419ok","task_id":null,"task_sub_type":"0","vid":"210012913_943f8db7688c46d48c3bc850f624281b","vod2Flag":"1"},{"appid":"1254094533","create_time":"2017-11-03 18:02:18","end_time":"2017-11-03 18:02:18","err_code":"0","expire_time":"2038-01-19 11:14:07","file_format":"1","file_id":"9031868223471114506","file_size":"489338","id":"419007019","record_file_id":"9031868223471114506","record_file_url":"http://1254094533.vod2.myqcloud.com/72067003vodgzp1254094533/6ad44dfc9031868223471114506/f0.flv","record_type":null,"report_message":null,"start_time":"2017-11-03 18:02:08","stream_id":"10906_M-8og1j9i419ok","task_id":null,"task_sub_type":"0","vid":"210012913_52317cbe1bd34a7a91fe6d383b23f143","vod2Flag":"1"}]},"ret":0,"retcode":0}
// 刪除檔案用 output.file_list[i].file_id    删除视频api: DeleteVodFile
//https://vod.api.qcloud.com/v2/index.php?Action=DeleteVodFile
//&fileId=16092504232103571364&priority=0&COMMON_PARAMS
                                                        //

                                                    });
                                                })
                                            })
                                        }
                                    })
                                })
                            }
                        }
                    })
                })
            }
        })

        // console.log("*****5.1. 檢查課程單元已結束是否已寫入Recordfile");
        models.Courseunit.findAll({where:{State:["E"],Videourl:'',Roomid:{$like:'M-%'}}}).then(function (docs) {
            if (docs!=null && docs.length>0){
                docs.forEach(function (unit) {
                    models.Recordfile.findOne({where:{CourseUnitId:unit.id}}).then(function (record) {
                        var tmp={"output":{"all_count":0,"file_list":[]}};
                        if (record!=null ){
                            try{
                                tmp= JSON.parse(record.logstr);
                            }
                            catch (e){
                                tmp={"output":{"all_count":0,"file_list":[]}};
                            }
                        }
                        if (record==null || record.logstr=="time expired" || (record.state==null && tmp.output.all_count==0)){
                            var chkday=moment().add(-3,"Days").tz("Asia/Taipei");
                            var starttime=moment(unit.Starttime).format("YYYY-MM-DD HH:mm:ss");
                            var channelid=wqcloud.bizId+"_"+ unit.Roomid;
                            wqcloud.Live_Tape_GetFilelist(channelid,starttime,function (res) {
                                if (record==null){
                                    record=new models.Recordfile();
                                    record.CourseunitId=unit.id;
                                }
                                record.logstr=res;
                                if (unit.Starttime<chkday && res=="time expired"){
                                    record.state="N"; //超過兩天沒取得錄影檔則不處理
                                    // console.log("*超過三天沒取得錄影檔-*"+unit.Roomid)
                                }
                                record.save();
                            })
                        }
                    })
                })
            }
        })

        // console.log("*****5.2. Recordfile影片數量大於0的 concatvideo");
        models.Recordfile.findAll({where:{state:null},include:[{model:models.Courseunit}]}).then(function (docs) {
            if (docs!=null && docs.length>0){
                // console.log("檢查課程單元已結束未產生錄影檔:共"+docs.length);
                docs.forEach(function (record) {
                    var tmp=(record.logstr=="time expired")?{"output":{"all_count":0,"file_list":[]}}: JSON.parse(record.logstr);
                    if (tmp.output.all_count>0){
                        var video=tool.filterData(tmp.output.file_list,{file_format: "3"});
                        var filename=record.Courseunit.Roomid;
                        var urls=[];
                        if (video!=null && video.length>0){
                            var files=[];
                            for (var i=0;i<video.length;++i){
                                files.push(video[i].record_file_id);
                                urls.push(video[i].record_file_url);
                            }
                            if (files.length==1){
                                record.Courseunit.Videourl=urls[0];
                                record.Courseunit.save();
                                record.state="Y";
                                record.save();
                                tool.sendPushFinishVideo(record.Courseunit.id);
                            }
                            else{
                                wqcloud.ConcatVideo(files,filename,function (res) {
                                    // console.log(res);
                                    if (res["code"]==0){
                                        record.state="C";
                                        record.save();
                                    }
                                })
                            }
                        }
                    }
                })
            }
        })

        // console.log("*****5.3. Recordfile已串接影片的資料查詢生成影片網址");
        models.Recordfile.findAll({where:{state:"C"},include:[{model:models.Courseunit}]}).then(function (docs) {
            if (docs!=null && docs.length>0){
                console.log("課程結束,已生成錄影檔:共"+docs.length);
                docs.forEach(function (record) {
                    var unit=record.Courseunit;
                    wqcloud.DescribeVodPlayInfo(unit.Roomid,function (res0) {
                        if (res0["code"]==0){
                            var videourl="";
                            for (var i=0;i<res0["fileSet"].length;++i){
                                if (res0["fileSet"][i]["playSet"][0].url.indexOf('.mp4')>0){
                                    videourl=res0["fileSet"][i]["playSet"][0].url;
                                    break;
                                }
                            }
                            if (videourl!=""){
                                unit.Videourl=videourl;
                                unit.save();
                                record.state="Y";
                                record.save();
                                tool.sendPushFinishVideo(unit.id);
                            }
                        }
                    })
                })
            }
        })
    })
    //
    schedulejob.scheduleJob('0 10 1 * * *', function () {
        console.log("*****5. 每日新增Monthreport");
        var sdate=moment(moment().add(-1,"Days")).format("YYYY-MM-DD")+" 00:00";
        var startdate=moment(sdate);//.add(16,"Hours");
        models.Monthreport.findOne({where:{Sumdate:sdate}}).then(function (doc) {
            if (doc==null){
                tool.dayBenefit(startdate,function (data) {
                    models.Monthreport.create(data).then(function () {
                        // console.log("新增Monthreport...success");
                    }).catch(function (err) {
                        console.log(err.message)
                    })
                })
            }
        })
        var dd=moment().tz("Asia/Taipei").format("DD");
        if (dd=="01"){
            // console.log("***** 6.每月老師出金報表");
            var cb1cnt=1;
            var total=0;
            try{
                var yyyymm=moment().tz("Asia/Taipei").format("YYYYMM");
                var qry="SELECT distinct(memberid) FROM Transactionlogs where memberid is not null and  transcode in ('E','T','S','J','P','U') and Paydate is null";
                tool.query(qry,function (arr) {
                    // console.log(arr);
                    cb1cnt=arr.length;
                    total=arr.length;

                    arr.forEach(function (item) {
                        models.Teacherpaylog.findOne({where:{YYYYMM:yyyymm,MemberId:item.memberid}}).then(function (doc) {
                            if (doc==null){
                                models.Teacher.findOne({where:{MemberId:item.memberid}}).then(function (teacher) {
                                    if (teacher!=null){
                                        models.Transactionlog.findAll({where:{MemberId:teacher.MemberId, Transcode:["E","T","S","J","P",'U'],Paydate:null},order:[[ "createdAt","ASC"]]}).then(function (docs) {
                                            if (docs!=null && docs.length>0){
                                                var sum=docs[0].Mcoin;
                                                for (var j=1;j<docs.length;++j){
                                                    sum+=docs[j].Mcoin;
                                                }
                                                var addw={YYYYMM:yyyymm,Amount:sum,Remain:teacher.Mcoin,MemberId:teacher.MemberId}; //出金時才出一筆W 負項 的transactionlog
                                                models.Teacherpaylog.create(addw).then(function (w) {
                                                    models.Transactionlog.update({PaylogId:w.id},{where:{MemberId:w.MemberId, Transcode:["E","T","S","J","P",'U'],Paydate:null}}).then(function () {
                                                        chkCompleteCB1();
                                                    })
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    })
                })
            }
            catch (err){
                console.log("***** 6.每月老師出金報表 作業失敗:"+err.message);
            }

            function chkCompleteCB1(){
                --cb1cnt;
                if (cb1cnt==0){
                    console.log("***** 6.每月老師出金報表 作業成功共:"+total+" 筆");
                }
            }
        }
    })
}

module.exports = schedule;