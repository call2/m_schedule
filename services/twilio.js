/**
 * Created by jack on 12/7/16.
 */
var express = require('express');
var router = express.Router();
var uniqid = require('uniqid');

// errors
var errors = require('../lib/errors');

// twilio
var Twilio=require('twilio');
var AccessToken = require('twilio').jwt.AccessToken;
var VideoGrant = AccessToken.VideoGrant;

// generate access token
var randtoken = require('rand-token');

var TWILIO_ACCOUNT_SID = "ACecd161f0ff875c37ebb0345cbd4f653b";
var TWILIO_API_KEY = "SKb69172501a113b4a82867b79fe005523";
var TWILIO_API_SECRET ="ZgkiZNfPW9m4LE5oygUl0zOAVjHvP0Bc";

/**
 * @api {get} /twilio/genToken/v1 genToken
 * @apiName genToken
 * @apiGroup twilio
 *
 * @apiDescription generate twilio token
 *
 * @apiSuccess {number} ErrCode Error Code.
 * @apiSuccess {String} ErrMsg  Error Message.
 * @apiSuccess {String} Twilio Token.
 *
 * @apiSampleRequest /twilio/genToken/v1
 */
router.get('/genToken/v1', function (req, res, next) {

    var token = new AccessToken(TWILIO_ACCOUNT_SID, TWILIO_API_KEY, TWILIO_API_SECRET);

    token.identity =uniqid();// randtoken.uid(8);

    var grant = new VideoGrant();
    // grant.configurationProfileSid = TWILIO_CONFIGURATION_SID;
    grant.roomname = "testfromAPI" ;
    token.addGrant(grant);

    // var response = {};
    var response = {
        roomname:"testfromAPI",
        // identity: token.identity,
        token: token.toJwt()
    };

    errors.SetErrorResponse(response, "0");

    res.send(response);
});

/**
 * @api {get} /twilio/closeRoom/v1 closeRoom
 * @apiName closeRoom
 * @apiGroup twilio
 *
 * @apiDescription close twilio room
 *
 * @apiParam {String} Roomid 直播室ID
 *
 * @apiSuccess {number} ErrCode Error Code.
 * @apiSuccess {String} ErrMsg  Error Message.
 *
 * @apiSampleRequest /twilio/closeRoom/v1
 */
router.get('/closeRoom/v1', function (req, res, next) {
    var client = new Twilio(TWILIO_API_KEY, TWILIO_API_SECRET, {accountSid: TWILIO_ACCOUNT_SID});
    var query = require('url').parse(req.url,true).query;
    var Roomid = query.Roomid;
    console.log(Roomid);
    client.video.rooms(Roomid)
        .update({
            status: 'completed'
        }).then(function (room) {
            console.log(room);
        res.send(room.status);
    }).catch(function (err) {
        res.send(err.message);
    })
});

/**
 * @api {get} /twilio/getRoomlist/v1 getRoomlist
 * @apiName getRoomlist
 * @apiGroup twilio
 *
 * @apiDescription  get twilio room list
 *
 * @apiParam {String} Roomname 直播室name
 *
 * @apiSuccess {number} ErrCode Error Code.
 * @apiSuccess {String} ErrMsg  Error Message.
 *
 * @apiSampleRequest /twilio/getRoomlist/v1
 */
router.get('/getRoomlist/v1', function (req, res, next) {

    var client = new Twilio(TWILIO_API_KEY, TWILIO_API_SECRET, {accountSid: TWILIO_ACCOUNT_SID});
    var query = require('url').parse(req.url,true).query;
    var Roomname = query.Roomname;
    console.log(Roomname);
    client.video.rooms(Roomname).fetch().then(function (room) {
        console.log(room);
        res.send("OK");
    }).catch(function (err) {
        console.log(err.message);
        res.send(err.message);
    })
});

/**
 * @api {get} /twilio/getRoomlistbystatus/v1 getRoomlistbystatus
 * @apiName getRoomlistbystatus
 * @apiGroup twilio
 *
 * @apiDescription  get twilio room list by status
 *
 * @apiParam {String} Status 查詢狀態
 *
 * @apiSuccess {number} ErrCode Error Code.
 * @apiSuccess {String} ErrMsg  Error Message.
 *
 * @apiSampleRequest /twilio/getRoomlistbystatus/v1
 */
router.get('/getRoomlistbystatus/v1', function (req, res, next) {

    var client = new Twilio(TWILIO_API_KEY, TWILIO_API_SECRET, {accountSid: TWILIO_ACCOUNT_SID});
    var query = require('url').parse(req.url,true).query;
    var Status = query.Status;
    console.log(Status);
    client.video.rooms.list({status: Status}).then(function (room) {
        // console.log(room);
        if (room!=null && room.length>0){
            console.log(room.length);
            for(var i=0;i<room.length;++i){
                console.log(room[i].sid);
            }
        }
        res.send("OK");
    }).catch(function (err) {
        console.log(err.message);
        res.send(err.message);
    });
});

module.exports = router;