/**
 * Created by ralph on 7/25/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Transactionlog = sequelize.define("Transactionlog", {
        Transcode: DataTypes.STRING(2), //
        Mcoin: DataTypes.INTEGER, //
        Remain: DataTypes.INTEGER, //餘額(M幣)
        Description: DataTypes.STRING,
        Refcode: DataTypes.STRING, //關聯資料代碼
        Refids: DataTypes.INTEGER, //關聯資料id 用
        PaylogId: DataTypes.INTEGER, //出金紀錄關聯資料id
        Paydate:DataTypes.DATE,
        Seqno: DataTypes.INTEGER,
        Money: DataTypes.INTEGER
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});

    Transactionlog.associate = function(models) {
        Transactionlog.belongsTo(models.Member, {
            foreignKey: {
                allowNull: true
            }
        });
    }

    return Transactionlog;
};