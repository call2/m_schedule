/**
 * Created by ralph on 7/24/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Teacheraudit = sequelize.define("Teacheraudit", {
        Title: DataTypes.STRING,
        Picurl: DataTypes.STRING,
        Description: DataTypes.STRING,
        Videourl: DataTypes.STRING,
        Musictype: DataTypes.STRING,
        Status: DataTypes.STRING(1), //W待審 C待面試 F已審核
        Result: DataTypes.STRING(1) //N不通過 Y通過
    });

    Teacheraudit.associate = function(models) {
        Teacheraudit.hasMany(models.Teacherauditlog);
        Teacheraudit.belongsTo(models.Admin, {
            foreignKey: {
                allowNull: false
            }
        });
        Teacheraudit.belongsTo(models.Member, {
            foreignKey: {
                allowNull: false
            }
        });
    }

    return Teacheraudit;
};