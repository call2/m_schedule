/**
 * Created by ralph on 7/24/2017.  //課程變更項目: 上課日期、課程描述、單元簡介內容  //N不通過 Y通過
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Courseauditlog = sequelize.define("Courseauditlog", {
        Comment: DataTypes.STRING,
        Changeitem: DataTypes.STRING,
        Result: DataTypes.STRING
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});

    Courseauditlog.associate = function(models) {
        Courseauditlog.belongsTo(models.Admin, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });
        Courseauditlog.belongsTo(models.Course, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });
    }

    return Courseauditlog;
};