/**
 * Created by ralph on 7/25/2017.   一般會員購買課程優惠設定紀錄檔(後台用)
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Courserewardlog = sequelize.define("Courserewardlog", {
        Quota: DataTypes.INTEGER, //滿多少M幣
        Reward: DataTypes.INTEGER, //回饋多少M幣
        Status:DataTypes.STRING(1) //Y啟用 N停用
    });

    Courserewardlog.associate = function(models) {
        Courserewardlog.belongsTo(models.Admin, {
            foreignKey: {
                allowNull: true
            }
        });
    }

    return Courserewardlog;
};