/**
 * Created by ralph on 7/24/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Expertise = sequelize.define("Expertise", {
        Expertisename: DataTypes.STRING, //音樂專長
        Expertisecolor: DataTypes.STRING //音樂類別色碼
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});
    Expertise.associate = function(models) {
        Expertise.hasMany(models.Memberexpertise);
        // Expertise.hasMany(models.Courseexpertise);
    }
    return Expertise;
};