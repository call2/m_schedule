/**
 * Created by ralph on 9/11/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Monthreport = sequelize.define("Monthreport", {
        Sumdate: DataTypes.DATE, //結帳日期
        Col0: DataTypes.INTEGER, //單元的價格(客戶要求加入的)
        Col1: DataTypes.INTEGER, //購買課程人次
        Col2: DataTypes.INTEGER, //學生退課入帳人次
        Col3: DataTypes.INTEGER, //M幣售出
        Col4: DataTypes.INTEGER, //Apple抽成金額
        Col5: DataTypes.INTEGER, //GooglePlay抽成金額
        Col6: DataTypes.INTEGER, //智付寶抽成金額
        Col7: DataTypes.INTEGER, //M幣回饋(公司)
        Col8: DataTypes.INTEGER, //老師收益(-優惠出帳+退課優惠入帳)
        Col9: DataTypes.INTEGER, //老師收益(-優惠出帳+退課優惠入帳)
        Col10: DataTypes.INTEGER,  //VIP收益
        Col11: DataTypes.INTEGER  //優惠劵出帳
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});

    return Monthreport;
};