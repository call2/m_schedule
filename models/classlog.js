/**
 * Created by ralph on 7/24/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Classlog = sequelize.define("Classlog", {
        Online: DataTypes.STRING(1), //W wait Y online N offline F finish
        Status: DataTypes.STRING(1) //Y valid X 退款
    });

    Classlog.associate = function(models) {
        Classlog.belongsTo(models.Courseunit, {
            foreignKey: {
                allowNull: false
            }
        });
        Classlog.belongsTo(models.Member, {
            foreignKey: {
                allowNull: false
            }
        });
    }

    return Classlog;
};