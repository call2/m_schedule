/**
 * Created by ralph on 7/24/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Teacherauditlog = sequelize.define("Teacherauditlog", {
        Comment: DataTypes.STRING,
        Result: DataTypes.STRING(1) //N不通過 Y通過
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});

    Teacherauditlog.associate = function(models) {
        Teacherauditlog.belongsTo(models.Member, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });
        Teacherauditlog.belongsTo(models.Teacheraudit, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });
    }

    return Teacherauditlog;
};