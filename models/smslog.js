"use strict";

module.exports = function(sequelize, DataTypes) {
    var Smslog = sequelize.define("Smslog", {
        Content: DataTypes.STRING,
        Mobile: DataTypes.STRING,
        kmsgid: DataTypes.STRING,
        State: DataTypes.STRING(1) //Y成功 N失敗
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});

    Smslog.associate = function(models) {
        Smslog.belongsTo(models.Member, {
            foreignKey: {
                allowNull: false
            }
        });
    }

    return Smslog;
};