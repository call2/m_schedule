/**
 * Created by ralph on 7/25/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Coupon = sequelize.define("Coupon", {
        Code: DataTypes.STRING, //優惠碼
        Mcoin: DataTypes.INTEGER, //M幣
        Duedate: DataTypes.DATE, //有效期限
        Allowuser: DataTypes.INTEGER, //允許使用人數
        Usercnt: DataTypes.INTEGER, //已使用人數
        State:DataTypes.STRING(1) //狀態 Y可使用 N不可使用
    });
    Coupon.associate = function(models) {
        Coupon.belongsTo(models.Admin, {
            foreignKey: {
                allowNull: false
            }
        });
    }
    return Coupon;
};