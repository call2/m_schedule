/**
 * Created by ralph on 7/24/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Courseunit = sequelize.define("Courseunit", {
        Unitno: DataTypes.INTEGER, //單元的序號(只有一個單元的話系統自動產生一筆)
        Unittitle: DataTypes.STRING, //單元標題
        Unitdate: DataTypes.DATE, //上課日期
        Starttime: DataTypes.DATE, //開始上課時間
        Unitprice: DataTypes.INTEGER, //單元的價格(客戶要求加入的)
        Introduction: DataTypes.STRING, //單元描述
        Videourl: DataTypes.STRING, //單元影片 or 直播url or 直播錄影url
        Roomid: DataTypes.STRING, //直播室ID(自行給定)
        Roomsid: DataTypes.STRING, //twilio room sid
        State: DataTypes.STRING(1) //W老師尚未按下[ready]  R老師按下[ready] E老師按下[結束] P
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});

    Courseunit.associate = function(models) {
        Courseunit.belongsTo(models.Course, {
            foreignKey: {
                allowNull: false
            }
        });
        Courseunit.hasMany(models.Classlog);
    }

    return Courseunit;
};