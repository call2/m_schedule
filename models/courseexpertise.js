/**
 * Created by ralph on 7/24/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Courseexpertise = sequelize.define("Courseexpertise", {
    });
    Courseexpertise.associate = function(models) {
        Courseexpertise.belongsTo(models.Course, {
            foreignKey: {
                allowNull: false
            }
        });
        Courseexpertise.belongsTo(models.Expertise, {
            foreignKey: {
                allowNull: false
            }
        });
    }
    return Courseexpertise;
};