/**
 * Created by ralph on 7/25/2017.  推播紀錄檔
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Pushlog = sequelize.define("Pushlog", {
        Type: DataTypes.STRING(1), //A課程變更  B課程即將開始  C訂閱的老師有新課程 D課程審核 E老師審核 F成為老師審核結果 GVIP會員期限到期通知 H審核員接收通知 Q課程問與答
        Content: DataTypes.STRING,
        Picurl: DataTypes.STRING,
        Title: DataTypes.STRING,
        Refid:DataTypes.INTEGER, //A,D,E,H :courseid(課程) B:courseunit.id(課程單元)  C:teacherid(老師) Q:qalogid(問與答)  F:X G:X,
        State: DataTypes.STRING(1) //H隱藏
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});

    Pushlog.associate = function(models) {
        Pushlog.belongsTo(models.Member, {
            foreignKey: {
                allowNull: false
            }
        });
    }

    return Pushlog;
};