/**
 * Created by ralph on 7/24/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Admin = sequelize.define("Admin", {
        Account: DataTypes.STRING,
        Password: DataTypes.STRING,
        Token: DataTypes.STRING,
        PS: DataTypes.STRING,
        Type: DataTypes.STRING(1) //0:最高權限 1:不可增加管理者
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});

    Admin.associate = function(models) {
        Admin.hasMany(models.Teacheraudit);
        Admin.hasMany(models.Courseauditlog);
        Admin.hasMany(models.Teacherbenefitratelog);
        Admin.hasOne(models.Feesetup);
    }

    return Admin;
};