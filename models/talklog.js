/**
 * Created by ralph on 7/25/2017.  直播留言紀錄檔
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Talklog = sequelize.define("Talklog", {
        Content: DataTypes.STRING,
        State: DataTypes.STRING(1) //H隱藏
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});

    Talklog.associate = function(models) {
        Talklog.belongsTo(models.Member, {
            foreignKey: {
                allowNull: false
            }
        });
        Talklog.belongsTo(models.Courseunit, {
            foreignKey: {
                allowNull: false
            }
        });
    }

    return Talklog;
};