/**
 * Created by ralph on 7/24/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Feesetup = sequelize.define("Feesetup", {
        Exchangerate: {type:DataTypes.FLOAT,defaultValue:1}, //M幣兌換率
        Benefitrate: DataTypes.INTEGER, //預設老師分潤
        Monthrate: DataTypes.INTEGER, //VIP月費
        Quarterrate: DataTypes.INTEGER, //VIP季費
        Yearrate: DataTypes.INTEGER, //VIP年費
        Lowestprice: {type:DataTypes.INTEGER,defaultValue:1}  //課程最低費用
    });

    Feesetup.associate = function(models) {
        Feesetup.belongsTo(models.Admin, {
            foreignKey: {
                allowNull: false
            }
        });
    }

    return Feesetup;
};