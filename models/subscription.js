/**
 * Created by ralph on 7/24/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Subscription = sequelize.define("Subscription", {
    });

    Subscription.associate = function(models) {
        Subscription.belongsTo(models.Member, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });
        Subscription.belongsTo(models.Teacher, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });
    }
    return Subscription;
};