/**
 * Created by ralph on 7/21/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var MemberPrivacy = sequelize.define("MemberPrivacy", {
        ShowEmail: DataTypes.STRING(1),
        ShowMobile: DataTypes.STRING(1),
        ShowFacebook: DataTypes.STRING(1),
        ShowLineid: DataTypes.STRING(1),
        ShowInstagramid: DataTypes.STRING(1),
        ShowTwitterid: DataTypes.STRING(1),
        ShowWeblogid: DataTypes.STRING(1),
        Bankname: DataTypes.STRING, //銀行名稱
        Bankid: DataTypes.STRING, //分行名稱及代碼
        Accountid: DataTypes.STRING, //銀行帳號
        Accountname: DataTypes.STRING, //戶名
        Passbookurl: DataTypes.STRING //存摺照面url
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});

    MemberPrivacy.associate = function(models) {
        MemberPrivacy.belongsTo(models.Member, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });
    }

    return MemberPrivacy;
};