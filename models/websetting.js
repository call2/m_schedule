/**
 * Created by ralph on 7/25/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Websetting = sequelize.define("Websetting", {
        Serviceterm: DataTypes.TEXT, //服務條款
        Privacy: DataTypes.TEXT, //隱私權政策
        Caution: DataTypes.TEXT, //成為老師注意事項
        Mcoin: DataTypes.TEXT //使用M幣注意事項
    });
    Websetting.associate = function(models) {
        Websetting.belongsTo(models.Admin, {
            foreignKey: {
                allowNull: false
            }
        });
    }
    return Websetting;
};