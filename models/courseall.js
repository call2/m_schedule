"use strict";

module.exports = function(sequelize, DataTypes) {
    var Courseall = sequelize.define("Courseall", {
        logstr: DataTypes.TEXT
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});

    Courseall.associate = function(models) {
        Courseall.belongsTo(models.Course, {
            foreignKey: {
                allowNull: false
            }
        });
    }

    return Courseall;
};