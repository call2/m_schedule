/**
 * Created by ralph on 9/18/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Teacherpaylog = sequelize.define("Teacherpaylog", {
        YYYYMM: DataTypes.STRING, //年月
        Amount: DataTypes.INTEGER, //出帳金額
        Remain: DataTypes.INTEGER, //出帳當時老師餘額
        Paytype:DataTypes.STRING(1), //銀行匯款,智付通,信用卡,現金
        Note:DataTypes.STRING, //註記
        Paytime:DataTypes.DATE, //付款日期
        AdminId: DataTypes.INTEGER //MemberId
    });

    Teacherpaylog.associate = function(models) {
        Teacherpaylog.belongsTo(models.Member, {
            foreignKey: {
                allowNull: false
            }
        });
    }

    return Teacherpaylog;
};