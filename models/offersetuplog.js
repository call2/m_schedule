/**
 * Created by ralph on 7/25/2017. 官方儲值優惠設定紀錄檔
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Offersetuplog = sequelize.define("Offersetuplog", {
        Startdate: DataTypes.DATE,
        Enddate: DataTypes.DATE,
        Price1: DataTypes.INTEGER, //購買M幣級距1
        Price2: DataTypes.INTEGER, //購買M幣級距2
        Price3: DataTypes.INTEGER, //購買M幣級距3
        Price4: DataTypes.INTEGER, //購買M幣級距4
        Price5: DataTypes.INTEGER, //購買M幣級距5
        Price6: DataTypes.INTEGER, //購買M幣級距6
        Price7: DataTypes.INTEGER, //購買M幣級距7
        Price8: DataTypes.INTEGER, //購買M幣級距8
        Exrate1: DataTypes.INTEGER, //210NT/M幣 兌換率
        Exrate2: DataTypes.INTEGER, //1200NT/M幣 兌換率
        Exrate3: DataTypes.INTEGER, //2400NT/M幣 兌換率
        Exrate4: DataTypes.INTEGER, //3600NT/M幣 兌換率
        Exrate5: DataTypes.INTEGER, //4800NT/M幣 兌換率
        Exrate6: DataTypes.INTEGER, //6000NT/M幣 兌換率
        Exrate7: DataTypes.INTEGER, //7200NT/M幣 兌換率
        Exrate8: DataTypes.INTEGER  //9000NT/M幣 兌換率
    });

    Offersetuplog.associate = function(models) {
        Offersetuplog.belongsTo(models.Admin, {
            foreignKey: {
                allowNull: true
            }
        });
    }

    return Offersetuplog;
};