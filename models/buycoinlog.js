/**
 * Created by ralph on 7/25/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Buycoinlog = sequelize.define("Buycoinlog", {
        Type: DataTypes.STRING, //A android B apple C智付寶
        Transid: DataTypes.STRING, //交易序號(金流平台傳回)
        Price: DataTypes.INTEGER, //金額
        Mcoin: DataTypes.INTEGER, //M幣
        Status:DataTypes.STRING(1) //交易結果 Y成功 N失敗
    });

    Buycoinlog.associate = function(models) {
        Buycoinlog.belongsTo(models.Member, {
            foreignKey: {
                allowNull: false
            }
        });
    }

    return Buycoinlog;
};