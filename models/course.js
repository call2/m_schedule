/**
 * Created by ralph on 7/24/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Course = sequelize.define("Course", {
        Title: DataTypes.STRING, //課程標題
        Picurl: DataTypes.STRING, //圖片網址
        Videourl: DataTypes.STRING, //試教影片網址
        Videotype: DataTypes.STRING, // 影片類型 Y:youtube S:S3
        Coursedate: DataTypes.DATE, //開課日期
        Coursetime: DataTypes.STRING, //多堂課程必須同一時間(HH:mm)
        Coursetype: DataTypes.STRING(1), //S一對一  M一對多  V影片教學
        State: DataTypes.STRING(1), //W 待審 Y上架(開啟) N關閉
        Auditstate: DataTypes.STRING(1), //Y通過 N不通過
        Courseduration: DataTypes.INTEGER, //課程長度(分鐘)
        Unit: DataTypes.INTEGER, //上課單元數
        Price: DataTypes.INTEGER, //M幣
        Price1: DataTypes.INTEGER, //錄影課程M幣(當Coursetype為 S or M 時)
        Quota: DataTypes.INTEGER, //人數限制
        Membertype: DataTypes.STRING(1), //M一般會員 V vip
        Brief: DataTypes.TEXT, //課程的簡介	300字以內
        Unitno: {type:DataTypes.INTEGER,defaultValue:0} //目前進行到第幾單元? (default 0 ,排程推送通知時新增classlog同時加1)
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});

    Course.associate = function(models) {
        Course.hasMany(models.Evaluation);
        Course.hasMany(models.Courseexpertise);
        Course.hasMany(models.Courseunit);
        Course.hasMany(models.Coursematerial);
        Course.hasMany(models.Courseauditlog);
        Course.hasMany(models.Purchaselog);
        Course.hasMany(models.Coursecollect);
        Course.belongsTo(models.Teacher, {
            foreignKey: {
                allowNull: false
            }
        });
    }

    return Course;
};