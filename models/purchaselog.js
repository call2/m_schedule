/**
 * Created by ralph on 7/25/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Purchaselog = sequelize.define("Purchaselog", {
        Purchasetype: DataTypes.STRING, //L live R record V video(會員可購買直播課程結束後的錄影(R))
        Status: {type:DataTypes.STRING(1),defaultValue:"Y"}, //Y 購買 N 退訂
        Iscontinue: {type:DataTypes.STRING(1),defaultValue:"Y"},
        Price: DataTypes.INTEGER //金額
    });

    Purchaselog.associate = function(models) {
        Purchaselog.hasMany(models.Transactionlog);
        Purchaselog.belongsTo(models.Member, {
            foreignKey: {
                allowNull: false
            }
        });
        Purchaselog.belongsTo(models.Course, {
            foreignKey: {
                allowNull: false
            }
        });
    }

    return Purchaselog;
};