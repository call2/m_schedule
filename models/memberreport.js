/**
 * Created by ralph on 7/25/2017.  會員檢舉申訴紀錄檔
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Memberreport = sequelize.define("Memberreport", {
        Type: DataTypes.STRING(1), //A申訴老師 B問與答 C評論 D課程
        Reason: DataTypes.STRING, //舉報原因 AD為申訴內容  BC為1. 不當或不實內容  2. 不雅內容  3. 廣告  4. 其他
        Refid: DataTypes.INTEGER, //A teacherid, B qalog.id C evaluation.id D course.id
        Condition: DataTypes.STRING(1), //N未處理 Y已處理
        State: DataTypes.STRING(1) //H隱藏
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});

    Memberreport.associate = function(models) {
        Memberreport.belongsTo(models.Member, {
            foreignKey: {
                allowNull: false
            }
        });
        Memberreport.belongsTo(models.Admin, {
            foreignKey: {
                allowNull: true
            }
        });
    }

    return Memberreport;
};