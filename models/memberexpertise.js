/**
 * Created by ralph on 7/24/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Memberexpertise = sequelize.define("Memberexpertise", {
    });
    Memberexpertise.associate = function(models) {
        Memberexpertise.belongsTo(models.Expertise, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });
        Memberexpertise.belongsTo(models.Member, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });
    }
    return Memberexpertise;
};