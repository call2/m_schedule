/**
 * Created by ralph on 7/24/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Teacherbenefitratelog = sequelize.define("Teacherbenefitratelog", {
        Benefitrate: DataTypes.INTEGER //%
    });

    Teacherbenefitratelog.associate = function(models) {
        Teacherbenefitratelog.belongsTo(models.Teacher, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });
        Teacherbenefitratelog.belongsTo(models.Admin, {
            foreignKey: {
                allowNull: false
            }
        });
    }

    return Teacherbenefitratelog;
};