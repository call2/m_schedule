"use strict";

module.exports = function(sequelize, DataTypes) {
    var Recordfile = sequelize.define("Recordfile", {
        logstr: DataTypes.TEXT,
        state:DataTypes.STRING(1)
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});

    Recordfile.associate = function(models) {
        Recordfile.belongsTo(models.Courseunit, {
            foreignKey: {
                allowNull: false
            }
        });
    }

    return Recordfile;
};