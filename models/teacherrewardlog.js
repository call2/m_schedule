/**
 * Created by ralph on 7/24/2017.  購買課程老師優惠設定紀錄檔(老師 用)
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Teacherrewardlog = sequelize.define("Teacherrewardlog", {
        Quota: DataTypes.INTEGER, //滿多少M幣
        Reward: DataTypes.INTEGER, //回饋多少M幣
        Status:DataTypes.STRING(1) //Y啟用 N停用
    });

    Teacherrewardlog.associate = function(models) {
        Teacherrewardlog.belongsTo(models.Teacher, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });
    }

    return Teacherrewardlog;
};