/**
 * Created by ralph on 7/24/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Teacher = sequelize.define("Teacher", {
        Subscriptioncnt:{type: DataTypes.INTEGER, defaultValue:0 }, //訂閱人數
        Gradeavg: {type: DataTypes.DECIMAL, defaultValue:0 }, //平均評分
        Benefitrate: {type: DataTypes.INTEGER , defaultValue: 30 }, //老師分潤比例
        Mcoin: {type: DataTypes.INTEGER , defaultValue:0 }, //課程收入M幣
        State: DataTypes.STRING(1), //Y啟用 N停權(停權後不可新增課程，可將舊有課程繼續教完) D:解除(取消會員的老師身份，視同一般會員，不可教學，老師若要重回老師身份，需透過前端「成為老師」重新送出申請，審核通過後方可成為老師
                                    // 一對一、一對多課程：退費給購買學生   影片教學：會員可繼續購買，收益歸Muzky所有
        Adminid: DataTypes.INTEGER,  //修改分潤管理員
        Rewardsetting:{type:DataTypes.STRING(1),defaultValue:"N"}
    });

    Teacher.associate = function(models) {
        Teacher.hasMany(models.Course);
        Teacher.hasMany(models.Teacherrewardlog);
        Teacher.hasMany(models.Teacherbenefitratelog);
        Teacher.hasMany(models.Evaluation);
        Teacher.hasMany(models.Subscription);
        Teacher.belongsTo(models.Member, {
            foreignKey: {
                allowNull: false
            }
        });
    }

    return Teacher;
};