/**
 * Created by ralph on 7/21/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Member = sequelize.define("Member", {
        Email: DataTypes.STRING,
        Memberlevel: DataTypes.STRING, //0:member 1:vip
        Account: DataTypes.STRING,
        Password: DataTypes.STRING,
        Nickname: DataTypes.STRING,
        Mobile: DataTypes.STRING,
        Facebookid: DataTypes.STRING,
        Facebookurl: DataTypes.STRING,
        Confirmcode: DataTypes.STRING,
        SendConfirmtime: DataTypes.DATE,
        PasswordConfirmcode: DataTypes.STRING,
        MobileConfirmcode: DataTypes.STRING,
        MobileConfirmdate: DataTypes.DATE,
        HeadPic: DataTypes.STRING,
        Selfinstroduction: DataTypes.STRING,
        Isauditor: DataTypes.STRING(1), //Y or N
        Isteacher: DataTypes.STRING(1), //Y or N
        Status: DataTypes.STRING, //Y啟用  N停權 W未驗證
        Mcoin: DataTypes.INTEGER,
        Sex: DataTypes.STRING, //0:女 1:男  2:其他
        Birthday: DataTypes.DATE,
        Lineid: DataTypes.STRING,
        Instagramid: DataTypes.STRING,
        Twitterid: DataTypes.STRING,
        Weblogid: DataTypes.STRING,
        Vipduedate: DataTypes.DATE, //VIP到期日
        Token: DataTypes.STRING(300),
        Devicetype: DataTypes.STRING,
        Pushtoken: DataTypes.STRING,
        TokenTerm: DataTypes.STRING,
        Pctoken: DataTypes.STRING,
        Googleid: DataTypes.STRING
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});

    Member.associate = function(models) {
        Member.hasOne(models.MemberPrivacy);
        Member.hasMany(models.Classlog);
        Member.hasMany(models.Memberexpertise);
        Member.hasMany(models.Evaluation);
        Member.hasMany(models.Teacheraudit);
        Member.hasMany(models.Teacherauditlog);
        Member.hasMany(models.Teacherauditmember);
        Member.hasMany(models.Subscription);
        Member.hasMany(models.Coursecollect);
        Member.hasMany(models.Buycoinlog);
        Member.hasMany(models.Purchaselog);
        Member.hasMany(models.Transactionlog);
    }

    return Member;
};
