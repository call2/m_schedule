/**
 * Created by ralph on 7/24/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Evaluation = sequelize.define("Evaluation", {
        Star: DataTypes.INTEGER,
        Content:  DataTypes.STRING, //評價內容
        Status: DataTypes.STRING(1) //設定隱藏後積分須重算
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});

    Evaluation.associate = function(models) {
        Evaluation.belongsTo(models.Member, {
            foreignKey: {
                allowNull: false
            }
        });
        Evaluation.belongsTo(models.Teacher, {
            foreignKey: {
                allowNull: false
            }
        });
        Evaluation.belongsTo(models.Course, {
            foreignKey: {
                allowNull: false
            }
        });
    }

    return Evaluation;
};