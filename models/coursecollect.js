/**
 * Created by ralph on 8/11/2017.
 */

module.exports = function(sequelize, DataTypes) {
    var Coursecollect = sequelize.define("Coursecollect", {
    });

    Coursecollect.associate = function(models) {
        Coursecollect.belongsTo(models.Member, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });
        Coursecollect.belongsTo(models.Course, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });
    }
    return Coursecollect;
};