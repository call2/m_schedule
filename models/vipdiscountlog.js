/**
 * Created by ralph on 7/25/2017.  VIP購買課程折扣設定紀錄檔
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Vipdiscountlog = sequelize.define("Vipdiscountlog", {
        State:DataTypes.STRING(1), //Y啟用
        Discount: DataTypes.INTEGER //%
    });

    Vipdiscountlog.associate = function(models) {
        Vipdiscountlog.belongsTo(models.Admin, {
            foreignKey: {
                allowNull: true
            }
        });
    }

    return Vipdiscountlog;
};