/**
 * Created by ralph on 7/24/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Coursematerial = sequelize.define("Coursematerial", {
        Attname: DataTypes.STRING, //教材名稱
        Atturl: DataTypes.STRING //教材網址
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});

    Coursematerial.associate = function(models) {
        Coursematerial.belongsTo(models.Course, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });
    }

    return Coursematerial;
};