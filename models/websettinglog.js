/**
 * Created by ralph on 7/25/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Websettinglog = sequelize.define("Websettinglog", {
        Type: DataTypes.STRING(1), //S:服務條款 P:隱私權政策 T:成為老師注意事項
        Content: DataTypes.TEXT //資料內容
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});
    Websettinglog.associate = function(models) {
        Websettinglog.belongsTo(models.Admin, {
            foreignKey: {
                allowNull: false
            }
        });
    }
    return Websettinglog;
};