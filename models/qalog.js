/**
 * Created by ralph on 7/25/2017.  問與答紀錄檔
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Qalog = sequelize.define("Qalog", {
        Qcontent: DataTypes.STRING, //問題內容
        Qtime: DataTypes.DATE, //問題時間
        Acontent: DataTypes.STRING, //回答內容
        Atime: DataTypes.DATE, //回答時間
        State: DataTypes.STRING(1) //H隱藏
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});

    Qalog.associate = function(models) {
        Qalog.belongsTo(models.Course, {
            foreignKey: {
                allowNull: false
            }
        });
        Qalog.belongsTo(models.Member, {
            foreignKey: {
                allowNull: false
            }
        });
        Qalog.belongsTo(models.Teacher, {
            foreignKey: {
                allowNull: true
            }
        });
        Qalog.belongsTo(models.Admin, {
            foreignKey: {
                allowNull: true
            }
        });
    }

    return Qalog;
};