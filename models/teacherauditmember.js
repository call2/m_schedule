/**
 * Created by ralph on 7/24/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Teacherauditmember = sequelize.define("Teacherauditmember", {
    });
    Teacherauditmember.associate = function(models) {
        Teacherauditmember.belongsTo(models.Teacheraudit, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });
        Teacherauditmember.belongsTo(models.Member, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });
    }
    return Teacherauditmember;
};