"use strict";

module.exports = function(sequelize, DataTypes) {
    var Apilog = sequelize.define("Apilog", {
        Testname: DataTypes.STRING,
        Api: DataTypes.STRING,
        Starttime: DataTypes.STRING,
        Err:DataTypes.STRING,
        Duration: DataTypes.INTEGER
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});

    return Apilog;
};