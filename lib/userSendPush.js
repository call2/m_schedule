/**
 * Created by ralph on 8/30/2017.
 */

var models  = require('../models');

//push
var PushService = require('../services/pushService');

function userSendPush() {

}

userSendPush.sendPushByOne = function(MemberID, Content, callback) {
    //傳送推播給單一一人
    // Refid: A,D,E,H :courseid(課程) B:courseunit.id(課程單元)  C:teacherid(老師) Q:qalogid(問與答)  F:X G:X,
    // Type: A課程變更  B課程即將開始  C訂閱的老師有新課程 D課程審核 E老師審核 F成為老師審核結果 G VIP會員期限到期通知 H審核員接收通知 Q課程問與答
    // Content: {type: String, required: true},
    models.Member.findOne({where:{id: MemberID}}).then(function (user) {
            if (user==null) {
                // return true;
                callback(false);
            } else {
                console.log(user.Pushtoken);
                if(user.Devicetype == "A"){
                    //Android
                    PushService.AndroidPush(user.Pushtoken, Content);
                    callback(true);
                } else  if(user.Devicetype == "I"){
                    //iOS
                    models.Pushlog.count({where:{MemberId:MemberID,State:"N"}}).then(function (notReadCount) {
                        PushService.iOSPush(user.Pushtoken, Content, notReadCount);

                    })
                    callback(true);
                }
            }
        });
};

userSendPush.sendPushByOne_SavePush = function(MemberID, Refid, Type,Title,Picurl ,Content, callback) {
    //傳送推播給單一一人並存擋
    // Refid: A,D,E,H :courseid(課程) B:courseunit.id(課程單元)  C:teacherid(老師) Q:qalogid(問與答)  F:X G:X J:X,
    // Type: A課程變更  B課程即將開始,影片錄製完成  C訂閱的老師有新課程 D課程審核 E老師審核 F成為老師審核結果 G VIP會員期限到期通知 H審核員接收通知 Q課程問與答 J:後台關閉(開啟)課程
    // Content: {type: String, required: true},
    models.Member.findOne({where:{id: MemberID}}).then(function(user) {
            if (user==null) {
                // return true;
                callback(false);
            } else {
                var notification = {
                    "MemberId":MemberID,
                    "Refid": Refid,
                    "Type": Type,
                    "Title": Title,
                    "Picurl":Picurl,
                    "State":"N",
                    "Content": Content
                }

                models.Pushlog.create(notification).then(function () {
                    userSendPush.sendPushByOne(user.id, Content, function (ok) {   //傳送推播給單一一人
                        // if(ok){
                        //     callback(true);
                        // } else {
                        //     callback(false);
                        // }
                    });
                    callback(true);
                }).catch(function (err) {
                    console.log("sendPushByOne_SavePush****error");
                    console.log(err.message);
                    callback(false);
                })
            }
        });
};

module.exports = userSendPush;