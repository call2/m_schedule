/**
 * Created by ralph on 8/7/2017.
 */
var models  = require('../models');

// mongoose schema - OTP
// var OTP = require('../models/otp');
// moment: date/time format
var moment = require('moment-timezone');
var validator = require('validator');
// Random String
var randomstring = require('randomstring');
var pushNotification = require('../lib/userSendPush');

function tool() {

}
tool.Admintype={"0":"最高管理員","1":"一般管理員"};
tool.ReportTxt={"1":"不當或不實內容","2":"不雅內容","3":"廣告","4":"其他"};
tool.ReporttypeTxt={"A":"老師","B":"課程","C":"問與答","D":"評價"};
tool.CoursetypeTxt={"S":"一對一視訊","M":"一對多直播","V":"影片教學"};
tool.TransTypeTxt={"A1":"Android手續費", "A2":"Apple內購手續費", "A3":"智付寶手續費", "D":"實收", "E":"優惠出帳(-)", "F":"優惠入帳(+)", "G":"購買課程", "T":"購買課程入帳(老師分潤)", "T1":"購買課程入帳(公司分潤)",
    "S":"老師遲到退款出帳","S1":"老師遲到退款(公司)出帳","I":"老師遲到退款入帳", "J":"學生退課出帳", "K":"學生退課入帳", "M":"VIP購買課程折扣出帳","N":"VIP購買課程折扣入帳","L":"儲值入帳","W":"老師出金出帳",
    "O":"購買直播錄影課程", "P":"購買直播錄影課程入帳(老師分潤)", "P1":"購買直播錄影課程入帳(公司分潤)", "Q":"購買VIP出帳", "R":"購買VIP入帳","U":"學生退課優惠入帳(老師)","U1":"學生退課優惠入帳(公司)",
    "V":"學生退課優惠出帳(學生)","Z":"會員Apple內購退款(會員M幣沖回)","Z1":"Apple內購退款(手續費沖回)","Z2":"Apple內購退款(公司實收沖回)","M1":"退訂VIP折扣公司沖回入帳","N1":"退訂VIP折扣會員沖回出帳",
    "X":"出金所得出帳","X1":"出金手續費出帳","X2":"出金所得稅出帳","Y":"優惠劵入帳","Y1":"優惠劵出帳(公司)"
};

tool.YMDHMS="YYYY.MM.DD HH:mm:ss";
tool.YMDHM="YYYY.MM.DD HH:mm";
tool.YMD="YYYY.MM.DD";
tool.YM="YYYY.MM";
tool.MD="MM.DD";

tool.defaultrate=80; //預設老師分潤比例
tool.exchangerate=3; //(NT*3 =1 Mcoin) 預設M幣兌換率

tool.getCouponcode=function (callback) {
    var code = randomstring.generate(6).toUpperCase();
    models.Coupon.findOne({where:{Code:code}}).then(function (doc) {
        if (doc==null){
            callback(code);
        }
        else {
            tool.getCouponcode(callback);
        }
    })
}
tool.getCoinList=function (callback) {
    var arr=[];
    models.Offersetuplog.findOne({order:[["createdAt","DESC"]]}).then(function (doc) {
        if (doc!=null){
            arr.push(doc.Price1);
            arr.push(doc.Price2);
            arr.push(doc.Price3);
            arr.push(doc.Price4);
            arr.push(doc.Price5);
            arr.push(doc.Price6);
            arr.push(doc.Price7);
            arr.push(doc.Price8);
        }
        callback(arr);
    })
}

tool.getMusicbyarr=function (arr,callback) {
    var res=[];
    if (arr==null||arr.length==0){
        callback(res);
    }
    else {
        for (var i=0;i<arr.length;++i){
            models.Expertise.findOne({where:{id:arr[i]}}).then(function (data) {
                var tmp={id:data.id,Expertisename:data.Expertisename,Expertisecolor:data.Expertisecolor};
                res.push(tmp);
                chkCompleteCB1(res);
            })
        }
    }
    var cb1cnt=arr.length;
    function chkCompleteCB1(response){
        --cb1cnt;
        if (cb1cnt==0){
            callback(response);
        }
    }
}

tool.chkMusictype = function( arr,callback){
    var res=true;
    var qry=[];
    for (var i=0;i<arr.length;++i){
        try{
            var tmpa=parseInt(arr[i]);
            qry.push({id:arr[i]});
        }
        catch (e){
            res=false;
            break;
        }
    }
    if (!res){
        callback(false);
    }
    else {
        models.Expertise.findAll({where:{$or:qry}}).then(function (docs) {
            if (docs.length!=qry.length){
                callback(false);
            }
            else {
                callback(true);
            }
        })
    }
}

tool.chkMusictypeIsused = function( id,callback){
    models.Courseexpertise.count({where:{ExpertiseId:id}}).then(function (cnt) {
        if (cnt>0){
            callback(true);
        }
        else {
            models.Memberexpertise.count({where:{ExpertiseId:id}}).then(function (cnt) {
                if (cnt>0){
                    callback(true);
                }
                else{
                    callback(false);
                }
            })
        }
    })
}

tool.arrCatstr=function (arr,sep) {
    var res="";
    for (var i=0;i<arr.length;++i){
        res+=arr[i].toString()+sep;
    }
    res=res.substring(0,res.length-1);
    return res;
}

tool.chkSubscription=function (userid,subarr) {
    var res="N";
    for(var j=0;j<subarr.length;++j){
        if (subarr[j].MemberId==userid){
            res="Y";
            break;
        }
    }
    return res;
}
tool.chkPurchasedByLog=function (userid,courseid,callback) {
    if (userid==null){
        callback(null)
    }
    else {
        models.Purchaselog.findOne({where:{MemberId:userid,CourseId:courseid,Status:"Y"},order:[["createdAt","DESC"]]}).then(function (log) {
            callback(log);
        })
    }
}

tool.chkPurchased=function (userid,subarr) {
    var res="N";
    for(var j=0;j<subarr.length;++j){
        if (subarr[j].MemberId==userid && subarr[j].Status=="Y" && (subarr[j].Purchasetype=="R" ||subarr[j].Purchasetype=="L" ||subarr[j].Purchasetype=="V") ){
            res="Y";
            break;
        }
    }
    return res;
}
tool.chkPurchasedrecord=function (userid,subarr) {
    var res="N";
    for(var j=0;j<subarr.length;++j){
        if (subarr[j].MemberId==userid && subarr[j].Status=="Y"){
            res="Y";
            break;
        }
    }
    return res;
}
tool.getBuysumandlog=function (ids,callback) {
    models.Purchaselog.findAll({where:{CourseId:ids,Status:["Y","N"]}}).then(function (docs) {
        var sum=0;
        if (docs.length==0){
            callback(0,null);
        }
        else {
            var arr=[];
            for (var i=0;i<docs.length;++i){
                sum+=docs[i].Price;
                arr.push(docs[i].id);
            }
            models.Transactionlog.findAll({where:{PurchaselogId:arr}}).then(function (logs) {
                callback(sum,logs);
            })
        }
    }).catch(function () {
        callback(0,null);
    })
}
tool.getBuycnt=function (courseid,callback) {
    models.Purchaselog.count({where:{CourseId:courseid,Status:"Y"}}).then(function (cnt) {
        callback(cnt);
    }).catch(function () {
        callback(0);
    })
}
tool.getRefundcnt=function (courseid,callback) {
    models.Purchaselog.count({where:{CourseId:courseid,Status:"N"}}).then(function (cnt) {
        callback(cnt);
    }).catch(function () {
        callback(0);
    })
}
tool.getPurchasetype=function (course,callback) {
    if (course.Coursetype=="V"){
        callback("V");
    }
    else {
        //檢查最後一堂課是否已結束
        models.Courseunit.findAll({where:{CourseId:course.id},order:[["Unitdate","DESC"]]}).then(function (docs) {
            if (docs.length==0){
                callback("");
            }
            else {
                if (docs[0].State=="E"){
                    callback("R");
                }
                else {
                    callback("L");
                }
            }
        })
    }
}
tool.getPurchaseprice=function (course,callback) {
    if (course.Coursetype=="V"){
        callback(course.Price1);
    }
    else {
        //檢查最後一堂課是否已結束
        models.Courseunit.findAll({where:{CourseId:course.id},order:[["Unitdate","DESC"]]}).then(function (docs) {
            if (docs.length==0){
                callback(-1);
            }
            else {
                if (docs[0].State=="E"){
                    callback(course.Price1);
                }
                else {
                    callback(course.Price);
                }
            }
        })
    }
}
tool.chkCoursecollect=function (userid,subarr) {
    var res=false;
    for(var j=0;j<subarr.length;++j){
        if (subarr[j].MemberId==userid){
            res=true;
            break;
        }
    }
    return res;
}
tool.getCollectcnt=function (courseid,callback) {
    models.Coursecollect.count({where:{CourseId:courseid}}).then(function (cnt) {
        callback(cnt);
    })
}
tool.getQacnt=function (courseid,callback) {
    models.Qalog.count({where:{CourseId:courseid}}).then(function (cnt) {
        callback(cnt);
    })
}
tool.getNumstr=function (inpnum) {
    if (inpnum>=1000000){
        return parseInt(inpnum/1000000) +"M";
    }
    else if (inpnum>=1000){
        return parseInt(inpnum/1000) +"K";
    }
    else
        return inpnum.toString();
}
tool.chkCoursetype=function (inpstr) {
    return (inpstr=="S"||inpstr=="M"||inpstr=="V");
}
tool.chkMembertype=function (inpstr) {
    return (inpstr=="M"||inpstr=="V");
}
tool.chkUnitdate=function (inparr) {
    for (var i=0;i<inparr.length;++i){
        try{
            if (!this.chkDate(inparr[i].Unitdate)) {
                return false;
            }
        }
        catch (e){
            return false;
        }
    }
    return true;
}
tool.chkClasslog=function(userid,unitids,callback) { //是否有退課
    models.Classlog.findOne({where:{MemberId:userid,CourseunitId:unitids}}).then(function (doc) {
        if (doc==null)
            callback("N");
        else
            callback(doc.Status);
    })
}
tool.chkBeenclass=function(userid,unitids,callback){ //是否上過課
    if (unitids.length==0){
        callback(0);
    }
    else {
        models.Classlog.count({where:{MemberId:userid,CourseunitId:unitids,Online:"Y"}}).then(function (cnt) {
            callback(cnt);
        })
    }
}
tool.Beenclasscnt=function(courseid,callback){ //上課人次
    models.Courseunit.findAll({where:{CourseId:courseid}}).then(function (docs) {
        if (docs==null||docs.length==0){
            callback(0);
        }
        else {
            var unitids=[];
            for (var i=0;i<docs.length;++i){
                unitids.push(docs[i].id);
            }
            models.Classlog.count({where:{CourseunitId:unitids,Status:["F","Y"]}}).then(function (cnt) {
                callback(cnt);
            })
        }
    })
}
tool.isEvaluate=function(userid,courseid,callback){ //是否上過課
    models.Evaluation.count({where:{MemberId:userid,CourseId:courseid}}).then(function (cnt) {
        if (cnt==0) callback("N"); else callback("Y");
    })
}
tool.getTeacherevalcnt=function(TeacherId,callback){
    models.Evaluation.count({where:{TeacherId:TeacherId,Status:"Y"}}).then(function (cnt) {
        callback(cnt);
    }).catch(function (err) {
        callback(0);
    })
}
tool.averageStar=function(TeacherId,star,callback){
    var res=0;
    models.Evaluation.findAll({where:{TeacherId:TeacherId,Status:"Y"}}).then(function (docs) {
        if (docs==null||docs.length==0){
            callback(star);
        }
        else {
            var cnt=docs.length;
            var sum=0;
            for (var i=0;i<docs.length;++i){
                sum+=docs[i].Star;
            }
            res=((sum+star)/(cnt+1)).toFixed(1) ;
            callback(res);
        }
    }).catch(function (err) {
        callback(0);
    })
}

//
tool.getTeacher=function (teacherid,callback) {
    models.Teacher.findOne({where:{id:teacherid}}).then(function (doc) {
        callback(doc);
    })
}
tool.isTeacher=function (userid,courseid,callback) {
    models.Course.findOne({where:{id:courseid}}).then(function (course) {
        if (course!=null){
            models.Teacher.findOne({where:{id:course.TeacherId}}).then(function (teacher) {
                if (teacher!=null){
                    callback(teacher.MemberId==userid);
                }
                else {
                    callback(false);
                }
            })
        }
        else{
            callback(false);
        }
    }).catch(function () {
        callback(false);
    })

}
tool.getTeachermemberid=function(teacherid,callback){
    models.Teacher.findOne({where:{id:teacherid}}).then(function (doc) {
        if (doc!=null){
            callback(doc.MemberId);
        }
        else
            callback(0);
    })
}

tool.getTeacherbenefitrate=function(teacherid,date,callback){
    models.Teacherbenefitratelog.findOne({where:{id:teacherid,createdAt:{$lt:date}},order:[["createdAt","DESC"]]}).then(function (doc) {
        if (doc!=null){
            callback(doc.Benefitrate);
        }
        else{
            models.Feesetup.findOne({where:{createdAt:{$lt:date}},order:[["createdAt","DESC"]]}).then(function (doc) {
                if (doc!=null){
                    callback(doc.Benefitrate);
                }
                else
                    callback(this.defaultrate);
            })
        }
    })
}
//
tool.getQalog=function (qaid,callback) {
    models.Qalog.findOne({where:{id:qaid}}).then(function (doc) {
        callback(doc);
    })
}
tool.getEvaluation=function (id,callback) {
    models.Evaluation.findOne({where:{id:id}}).then(function (doc) {
        callback(doc);
    })
}
tool.getCourse=function (id,callback) {
    models.Course.findOne({where:{id:id}}).then(function (doc) {
        callback(doc);
    })
}
//
tool.chkDate=function (inpstr) {
    if (inpstr!=null && inpstr!=""){
        var tmpstr=replaceAll(replaceAll(inpstr,"/","-"),".","-");
        var res= moment(tmpstr,"YYYY-MM-DD").isValid();
        return res;
    }
    else {
        return false;
    }
}
tool.chkDatearr=function (inparr) {
    for(var i=0;i<inparr.length;++i){
        if (!this.chkDate(inparr[i])){
            return false;
        }
    }
    return true
}
tool.getOffermessage=function (course,callback) {
    var res="";
    this.getPurchaseprice(course,function (price) {
        if (price>0){
            models.Teacherrewardlog.findOne({where:{Status:"Y",TeacherId:course.TeacherId},order:[["createdAt","DESC"]]}).then(function (treward) {
                if (treward!=null && treward.Status=="Y" && price>=treward.Quota ){
                    var tmp=parseInt(price/treward.Quota)
                    res+=",老師回饋"+treward.Reward*tmp+"M幣";
                }
                models.Courserewardlog.findOne({order:[["createdAt","DESC"]]}).then(function (creward) {
                    if (creward!=null && creward.Status=="Y" && price>=creward.Quota ){
                        var tmp=parseInt(price/creward.Quota)
                        res="官方回饋"+creward.Reward*tmp+"M幣"+res;
                    }
                    if (res!="") res="(購買此課程立即享有"+res+")";
                    callback(res);
                })
            })
        }
        else
            callback(res);
    })
}
tool.getOffercoin=function (course,callback) {
    var res=0;
    this.getPurchaseprice(course,function (price) {
        if (price!=-1){
            models.Teacherrewardlog.findOne({where:{Status:"Y",TeacherId:course.TeacherId},order:[["createdAt","DESC"]]}).then(function (treward) {
                if (treward!=null && treward.Status=="Y" && price>=treward.Quota ){
                    res+=treward.Reward;
                }
                models.Courserewardlog.findOne({order:[["createdAt","DESC"]]}).then(function (creward) {
                    if (creward!=null && creward.Status=="Y" && price>=creward.Quota ){
                        res+=creward.Reward;
                    }
                    callback(res);
                })
            })
        }
        else
            callback(res);
    })
}
tool.setDate=function (inpstr) {
    if (inpstr!=null && inpstr!=""){
        var tmpstr=replaceAll(replaceAll(inpstr,"/","-"),".","-");
        var res= moment(tmpstr,"YYYY-MM-DD HH:mm");
        return res;
    }
    else {
        return null;
    }
}
tool.strtoDate=function (inpstr) {
    if (inpstr!=null && inpstr!=""){
        var tmpstr=replaceAll(replaceAll(inpstr,"/","-"),".","-");
        var res= new Date(tmpstr);
        return res;
    }
    else {
        return null;
    }
}
tool.fmtDatestr=function (inpstr) {
    if (inpstr!=null && inpstr!=""){
        return replaceAll(replaceAll(inpstr,"/","-"),".","-");
    }
    else {
        return null;
    }
}
//每日收益報表
// "A":"Android手續費", "B":"Apple內購手續費", "C":"智付寶手續費", "D":"實收", "E":"優惠出帳(-)", "F":"優惠入帳(+)", "G":"購買課程", "T":"購買課程入帳(老師分潤)", "T1":"購買課程入帳(公司分潤)",
// "S":"老師遲到退款出帳","S1":"老師遲到退款(公司)出帳","I":"老師遲到退款入帳", "J":"學生退課出帳", "K":"學生退課入帳", "M":"VIP購買課程折扣出帳","N":"VIP購買課程折扣入帳","L":"儲值入帳",
// "O":"購買直播錄影課程", "P":"購買直播錄影課程入帳(老師分潤)", "P1":"購買直播錄影課程入帳(公司分潤)", "Q":"購買VIP出帳", "R":"購買VIP入帳","U":"學生退課優惠入帳(老師)","U1":"學生退課優惠入帳(公司)","V":"學生退課優惠出帳(學生)"
tool.dayBenefit=function (date,callback) {
    var startdate=moment(date);
    var enddate=moment(startdate).add(1,"Days").format("YYYY-MM-DD HH:mm:ss");
    var qry=" SELECT "+
        "(select count(*) from Courseauditlogs where createdAt>='"+moment(startdate).format("YYYY-MM-DD HH:mm:ss")+"' and createdAt<='"+enddate+"' and Result='Y' ) as cnt0,"+ //開課數
        "sum(case when Transcode='G' then 1 else 0 end) as cnta,"+ //購買課程人次
        "sum(case when Transcode='K' then 1 else 0 end) as cntb,"+ //學生退課入帳人次
        "sum(case when Transcode='L' then Mcoin else 0 end) as cntc,"+ //儲值入帳
        "sum(case when Transcode='J' and memberid is not null then Mcoin else 0 end) as cntc1,"+ //學生退課出帳(老師)
        "sum(case when Transcode='J' and memberid is null then Mcoin else 0 end) as cntc2,"+ //學生退課出帳(公司)
        "sum(case when Transcode='U' then Mcoin else 0 end) as cntc3,"+ //學生退課優惠入帳(老師)
        "sum(case when Transcode='U1' then Mcoin else 0 end) as cntc4,"+ //學生退課優惠入帳(公司)
        "sum(case when Transcode='A2' then Mcoin else 0 end) as cntd,"+ //Apple內購手續費
        "sum(case when Transcode='A1' then Mcoin else 0 end) as cnte,"+ //Android(Google play)手續費
        "sum(case when Transcode='A3' then Mcoin else 0 end) as cntf,"+ //智付寶手續費
        "sum(case when Transcode='E' and memberid is not null then Mcoin else 0 end) as cntg,"+ //優惠出帳(老師)
        "sum(case when Transcode='E' and memberid is null then Mcoin else 0 end) as cnth,"+ //優惠出帳(公司)
        "sum(case when Transcode in ('T','P') then Mcoin else 0 end) as cnti,"+ //老師收益
        "sum(case when Transcode in ('T1','P1') then Mcoin else 0 end) as cntj,"+ //公司收益
        "sum(case when Transcode='R' then Mcoin else 0 end) as cntk, "+ //VIP入帳
        "sum(case when Transcode='Y1' then Mcoin else 0 end) as cnty "+ //優惠劵出帳
        "from Transactionlogs where createdAt>='"+moment(startdate).format("YYYY-MM-DD HH:mm:ss")+"' and createdAt<='"+enddate+"'";
    // console.log(qry);
    this.query(qry,function (data) {
        var res={
            Sumdate:startdate,
            Col0:data[0].cnt0, //開課數量
            Col1:(data[0].cnta==null)?0:data[0].cnta, //購買課程人次
            Col2:(data[0].cntb==null)?0:data[0].cntb, //學生退課入帳人次
            Col3:(data[0].cntc==null)?0:data[0].cntc, //M幣售出 (** 不可算入公司收益,因為M幣還在user那裏)
            Col4:(data[0].cntd==null)?0:data[0].cntd, //Apple抽成金額
            Col5:(data[0].cnte==null)?0:data[0].cnte, //GooglePlay抽成金額
            Col6:(data[0].cntf==null)?0:data[0].cntf, //智付寶抽成金額
            Col7:-(parseInt((data[0].cnth==null)?0:data[0].cnth)+parseInt((data[0].cntc4==null)?0:data[0].cntc4)), //M幣回饋(公司)
            Col8:parseInt((data[0].cnti==null)?0:data[0].cnti)+(parseInt((data[0].cntg==null)?0:data[0].cntg)+parseInt((data[0].cntc3==null)?0:data[0].cntc3)), //老師收益(-優惠出帳+退課優惠入帳)
            Col9:parseInt((data[0].cntj==null)?0:data[0].cntj)+(parseInt((data[0].cnth==null)?0:data[0].cnth)+parseInt((data[0].cntc4==null)?0:data[0].cntc4)), //公司收益(-優惠出帳+退課優惠入帳)
            Col10:(data[0].cntk==null)?0:data[0].cntk,  //VIP收益
            Col11:(data[0].cnty==null)?0:data[0].cnty  //優惠劵出帳
        }
        callback(res);
    })
}

tool.monthGraphdata=function (date,callback) {
    var startdate=date;
    var enddate=moment(startdate).add(1,"Months");
    var qry="SELECT sum(Col0) as sum0,sum(Col1) as sum1,sum(Col8) as sum8,sum(Col9) as sum9 from Monthreports where sumdate>='"+moment(startdate).format("YYYY-MM-DD HH:mm")+
        "' and sumdate<='"+moment(enddate).format("YYYY-MM-DD HH:mm")+"'";
    this.query(qry,function (data) {
        var res={
            Sum0:(data[0].sum0==null)?0:data[0].sum0, //開課數量
            Sum1:(data[0].sum1==null)?0:data[0].sum1, //購買課程人次
            Sum8:(data[0].sum8==null)?0:data[0].sum8, //老師收益
            Sum9:(data[0].sum9==null)?0:data[0].sum9  //公司收益
        }
        callback(res);
    })
}

//維護Transactionlog的程式
tool.updateTranslog=function (memberid,callback) {
    models.Member.findOne({where:{id:memberid},include:[{model:models.Transactionlog,where:{Transcode:["L","F","G","I","K","O","Q"]}}],order:[[ models.Transactionlog,"createdAt","ASC"]]}).then(function (user) {
        if (user.Transactionlogs!=null && user.Transactionlogs.length>0){
            cb1cnt=user.Transactionlogs.length;
            var remain=user.Transactionlogs[0].Mcoin;
            for (var i=0;i<user.Transactionlogs.length;++i){
                var trs=user.Transactionlogs[i];
                remain+=trs.Mcoin;
                trs.Remain=remain;
                trs.save().then(function () {
                    chkCompleteCB1(remain);
                }).catch(function (err) {
                    callback(err.message);
                })
            }
        }
        else{
            callback("no transactionlog");
        }
    })
    var cb1cnt=0;
    function chkCompleteCB1(remain){
        --cb1cnt;
        if (cb1cnt==0){
            callback(remain);
        }
    }
}
tool.updateTeacherlog=function (memberid,callback) {
    models.Member.findOne({where:{id:memberid},include:[{model:models.Teacher},{model:models.Transactionlog,where:{ Transcode:["E","T","S","J","P","W"]}}],order:[[ models.Transactionlog,"createdAt","ASC"]]}).then(function (user) {
        if (user.Transactionlogs!=null && user.Transactionlogs.length>0 && user.Teacher!=null){
            cb1cnt=user.Transactionlogs.length;
            var remain=user.Transactionlogs[0].Mcoin;
            for (var i=0;i<user.Transactionlogs.length;++i){
                var trs=user.Transactionlogs[i];
                remain+=trs.Mcoin;
                trs.Remain=remain;
                trs.save().then(function () {
                    chkCompleteCB1();
                }).catch(function (err) {
                    callback(err.message);
                })
            }
        }
        else{
            callback("no transactionlog or this user is not a teacher");
        }
    })
    var cb1cnt=0;
    function chkCompleteCB1(){
        --cb1cnt;
        if (cb1cnt==0){
            callback("");
        }
    }
}

function replaceAll(str, find, replace)
{
    while( str.indexOf(find) > -1)
    {
        str = str.replace(find, replace);
    }
    return str;
}
tool.uniqArr=function(arr) {
    var a = [];
    for (var i=0, l=arr.length; i<l; i++)
        if (a.indexOf(arr[i]) === -1 && arr[i] !== '')
            a.push(arr[i]);
    return a;
}
//filter json data usage:var data = filterData(JSON.parse(my_json), {website: 'yahoo'});
tool.filterData=function(my_object, my_criteria){
    return my_object.filter(function(obj) {
        return Object.keys(my_criteria).every(function(c) {
            return obj[c] == my_criteria[c];
        });
    });
}

//filter another approach:
// use the Array.prototype.filter method:
//
//     homes.filter(function (el) {
//         return el.price <= 1000 &&
//             el.sqft >= 500 &&
//             el.num_of_beds >=2 &&
//             el.num_of_baths >= 2.5;
//     });

//sql
tool.query=function (qry,callback) {
    models.sequelize.query(qry,{ type: models.sequelize.QueryTypes.SELECT}).then(function (res) {
        callback(res);
    })
}
//

tool.sendPushFinishVideo=function(unitid,callback){
    models.Classlog.findAll({where:{CourseunitId:unitid,Status:"Y"},include:[{model:models.Courseunit,include:[{model:models.Course,include:[{model:models.Teacher}]}]}]}).then(function (docs) {
        if (docs!=null && docs.length>0){
            docs.forEach(function (log) {
                pushNotification.sendPushByOne_SavePush(log.MemberId,log.Courseunit.CourseId,"B","("+log.Courseunit.Course.Title+")影片錄製完成",log.Courseunit.Course.Picurl,"("+log.Courseunit.Course.Title+")影片錄製完成,按下[觀看]開始複習你的課程",function () {
                    callback(true);
                });
            })
        }
    })
}

module.exports = tool;