/**
 * Created by mac on 2017/2/27.
 */

var models  = require('../models');

// mongoose schema - OTP
// var OTP = require('../models/otp');
// moment: date/time format
var moment = require('moment-timezone');
var validator = require('validator');
var tool = require('./tool');

function userCheck() {

}

userCheck.checkMemberID = function(MemberID, callback) {
    models.Member.findOne({where:{Id: MemberID}}).then(function (docs) {
        if(docs != null){
            // return true;
            callback(true);
        } else {
            // return true;
            callback(false);
        }
    }).catch(function () {
        callback(false);
    })
};

userCheck.checkPhoneNumber=function (inputtxt) {
    // var phoneno =new regex('/^\d{10}$/') ;
    // var phoneno='/^\d{10}$/';
    if(inputtxt.value.match(/^\d{10}$/))
    {
        return true;
    }
    else
    {
        // alert("message");
        return false;
    }
}
//
// userCheck.checkNotificationIsRead = function(MemberID, callback) {
//     Member.findOne({$and:[{_id: MemberID}, {Notification: {$elemMatch: {IsRead: "N"}}}]},
//         function (err, docs) {
//             if (err) {
//                 // return true;
//                 callback(false);
//             } else {
//                 if(docs != null){
//                     // return true;
//                     callback(true);
//                 } else {
//                     // return true;
//                     callback(false);
//                 }
//             }
//         });
// };

// userCheck.getNotification = function(Token, callback) {
//     Member.findOne({Token: Token},
//         function (err, docs) {
//             if (err) {
//                 // return true;
//                 callback(false);
//             } else {
//                 if(docs != null){
//
//                     var notReadCount = 0;
//                     docs.Notification.forEach(function (doc) {
//                         if(doc.IsRead == "N"){
//                             notReadCount ++;
//                         }
//                     });
//
//                     docs.Notification.forEach(function (doc) {
//                         doc.IsRead = "Y";
//                     });
//
//                     docs.save(function (err) {
//                         if(err){
//                             callback(false);
//                         } else {
//                             callback(true, notReadCount);
//                         }
//                     });
//                 } else {
//                     // return true;
//                     callback(false);
//                 }
//             }
//         });
// };

userCheck.checkNickname = function(Nickname, callback) {
    // console.log("checkNickname:"+Nickname);
    models.Member.findOne({where:{Nickname: Nickname}}).then(function (docs) {
        // console.log(docs);
        if(docs!=null){
            callback(true);
        } else {
            callback(false);
        }
    })
    //     .catch(function (err) {
    //     console.log("checkNickname error:"+err.message)
    //     callback(false);
    // })
};

// userCheck.editNickname = function(Token, Nickname, callback) {
//     Member.find({$and:[{Nickname: Nickname}, {Token: {$ne: Token}}]},
//         function (err, docs) {
//             if (err) {
//                 // return true;
//             } else {
//                 if(docs.length > 0){
//                     // return true;
//                     callback(true);
//                 } else {
//                     // return true;
//                     callback(false);
//                 }
//             }
//         });
// };

var Sequelize=require("sequelize");
userCheck.checkEmail = function(Email, callback) {
    models.Member.findOne({where:Sequelize.where(Sequelize.fn('lower', Sequelize.col('Email')), Sequelize.fn('lower', Email))}).then(function (docs) {
        // console.log(docs);
        if(docs!=null){
            callback(true);
        } else {
            callback(false);
        }
    })
};

userCheck.checkAccount = function(Account, callback) {
    models.Member.findOne({where:{Account: Account}}).then(function (docs) {
        // console.log(docs);
        if(docs){
            callback(true);
        } else {
            callback(false);
        }
    }).catch(function () {
        callback(false);
    })
};

userCheck.checkEmailValidator = function(Email, callback) {
    if(validator.isEmail(Email)){
        //正確
        callback(true);
    } else {
        //錯誤
        callback(false);
    }
};

userCheck.checkMemberPhoneConfirm = function(Token, callback) {
    //檢查會員電話是否已驗證
    models.Member.findOne({where:{Token: Token}}).then(function (docs) {
        if(docs != null){
            if(docs.IsPhoneConfirm == "Y"){
                callback(true);
            } else {
                callback(false);
            }
        } else {
            callback(false);
        }
    }).catch(function () {
        callback(false);
    })
};

userCheck.checkFBID = function(FBID, callback) {
    models.Member.findOne({where:{FBID: FBID}}).then(function (docs) {
        if(docs.length > 0){
            callback(true);
        } else {
            callback(false);
        }
    }).catch(function () {
        callback(false);
    })
};

userCheck.checkMemberPhone = function(Phone, callback) {
    //檢查使用者輸入的Phone是否有人使用
    models.Member.findAll({where:{Phone: Phone}}).then(function(docs){
        if (docs.length>0){
            callback(true);
        }
        else {
            callback(false);
        }
    }).catch(function () {
        callback(false);
    })
};

userCheck.checkPhoneFormat = function(Phone) {
    //檢查使用者輸入的Phone是否為10碼
    if(Phone.length == 10){
        try{
            var tmp=parseInt(Phone);
            return true;
        }
        catch (err){
            return false;
        }
    } else {
        return false;
    }
};

userCheck.checkOTPCode = function(memberId, OTPCode, callback) {
    //檢查使用者輸入的驗證碼是否為正確
    models.Member.findOne({where:{Id: memberId,Confirmcode:OTPCode}}).then(function (docs) {
        if (docs.length>0){
            callback(true);
        }
        else {
            callback(false);
        }
    }).catch(function () {
        callback(false);
    });
};

userCheck.checkDeviceType = function(Type, callback) {
    //檢查使用者輸入的是否為Android or iOS
    if(Type == "A" || Type == "I"){
        callback(true);
    } else {
        callback(false);
    }
};

userCheck.checkReportType = function(Type, callback) {
    //
    if(Type == "1" || Type == "2" || Type == "3" ||
        Type == "4" || Type == "5" || Type == "6"){
        callback(true);
    } else {
        callback(false);
    }
};

userCheck.checkMemberSex = function(Sex, callback) {
    //性別 0:女 1:男  2:其他
    if(Sex == "0" || Sex == "1"|| Sex == "2"|| Sex == ""){
        callback(true);
    } else {
        callback(false);
    }
};
userCheck.getAccount = function(email,memberid, callback) {
    var account=email.split("@")[0];
    models.Member.findOne({where:{Account:account,id:{$ne:memberid}}}).then(function (user) {
        if (user!=null){
            callback(account+"."+memberid);
        }
        else {
            callback(account);
        }
    })
};

userCheck.checkToken = function(Token, callback) {
    if(Token == ""){
        callback(false);
    } else {
        models.Member.findOne({where:{$or:[{Token:req.body.Token},{Pctoken:req.body.Token}]}}).then(function (docs) {
            if(docs.length>0){
                callback(true, docs._id, docs.Nickname,docs.TeamCalendar,docs.CourseCalendar);
            } else {
                callback(false);
            }
        }).catch(function () {
            callback(false);
        })
    }
};
userCheck.checkYN=function (inparr) {
    for (var i=0;i<inparr.length;++i){
        if ("YN".indexOf(inparr[i])<0 ){
            return false;
        }
    }
    return true;
}
userCheck.getMemberInfo=function (Memberid,callback) {
    var memberinfo={};
    models.Member.findOne({where:{id: Memberid},include:[{model:models.MemberPrivacy},{model:models.Memberexpertise}]}).then(function (docs) {
        if (docs!=null){
            memberinfo["Mcoin"]=docs.Mcoin;
            memberinfo["Email"]=docs.Email;
            memberinfo["Memberlevel"]=docs.Memberlevel; //0:member 1:vip
            memberinfo["Account"]=docs.Email;// docs.Account;
            memberinfo["Nickname"]=docs.Nickname;
            memberinfo["Mobile"]=docs.Mobile;
            memberinfo["Facebookid"]=docs.Facebookurl;
            memberinfo["Confirmcode"]=docs.Confirmcode;
            memberinfo["SendConfirmtime"]=docs.SendConfirmtime;
            memberinfo["PasswordConfirmcode"]=docs.PasswordConfirmcode;
            memberinfo["HeadPic"]=docs.HeadPic;
            memberinfo["Selfinstroduction"]=docs.Selfinstroduction;
            memberinfo["Isauditor"]=docs.Isauditor;
            memberinfo["Isteacher"]=docs.Isteacher;
            memberinfo["Status"]=docs.Status; //Y啟用  N停權 W未驗證
            memberinfo["Mcoin"]=docs.Mcoin;
            memberinfo["Sex"]=docs.Sex;
            memberinfo["Birthday"]=(docs.Birthday==null)?null:moment(docs.Birthday).tz('Asia/Taipei').format(tool.YMD);
            memberinfo["Facebookurl"]=docs.Facebookurl;
            memberinfo["Lineid"]=docs.Lineid;
            memberinfo["Instagramid"]=docs.Instagramid;
            memberinfo["Twitterid"]=docs.Twitterid;
            memberinfo["Weblogid"]=docs.Weblogid;
            memberinfo["Vipduedate"]=(docs.Vipduedate==null)?null:moment(docs.Vipduedate).tz('Asia/Taipei').format(tool.YMDHM) ; //VIP到期日
            if (docs.MemberPrivacy!=null){
                memberinfo["ShowEmail"]=docs.MemberPrivacy.ShowEmail;
                memberinfo["ShowMobile"]=docs.MemberPrivacy.ShowMobile;
                memberinfo["ShowFacebook"]=docs.MemberPrivacy.ShowFacebook;
                memberinfo["ShowLineid"]=docs.MemberPrivacy.ShowLineid;
                memberinfo["ShowInstagramid"]=docs.MemberPrivacy.ShowInstagramid;
                memberinfo["ShowTwitterid"]=docs.MemberPrivacy.ShowTwitterid;
                memberinfo["ShowWeblogid"]=docs.MemberPrivacy.ShowWeblogid;
            }
            memberinfo["Memberexpertise"]=[];
            if (docs.Memberexpertises!=null){
                var arr=[];
                for (var i=0;i<docs.Memberexpertises.length;++i){
                    arr.push(docs.Memberexpertises[i].ExpertiseId);
                }
            }
            memberinfo["Memberexpertise"]=arr;
            callback(memberinfo);
        }
    }).catch(function () {
        callback(memberinfo);
    })
}
module.exports = userCheck;