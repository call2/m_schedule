/**
 * Created by mac on 2017/1/3.
 */

//npm install config.json
var errors = require('config.json')('lib/errors.json');
// fs
var fs = require('fs');

function Errors() {

}

Errors.GetErrorResponse = function () {
    // var obj = JSON.parse(fs.readFileSync('lib/errors.json', 'utf8'));
    // return obj;

    var obj = JSON.parse(fs.readFileSync('lib/errors.json', 'utf8'));

    var result = [];

    for (var i = 0; i < 1000; i++) {
        if (obj[i] != null) {
            result.push(obj[i]);
        }
    }

    return result;
};

Errors.SetErrorResponse = function (response, errorCode) {
    response['ErrCode'] = errors[errorCode].ErrCode;
    response['ErrMsg'] = errors[errorCode].ErrMsg;
};

Errors.SetBackErrorResponse = function (response, errorCode, errorField) {
    response['ErrCode'] =errors[errorCode].ErrCode;
    response['ErrMsg'] =(errorField==null||errorField=="")? errors[errorCode].ErrMsg: errors[errorCode].ErrMsg+": "+errorField;
};

module.exports = Errors;